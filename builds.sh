#!/bin/sh

header() {
  echo "$(tput setaf 6)$(tput bold)╔═════════════════════════════════╗ $(tput sgr 0)"
  echo "$(tput setaf 6)$(tput bold)║          Run commands:          ║ $(tput sgr 0)"
  echo "$(tput setaf 6)$(tput bold)╟─────────────────────────────────╢ $(tput sgr 0)"
  echo "$(tput setaf 6)$(tput bold)║       ➜ 1 | --build             ║ $(tput sgr 0)"
  echo "$(tput setaf 6)$(tput bold)║       ➜ 2 | --ssr               ║ $(tput sgr 0)"
  echo "$(tput setaf 6)$(tput bold)║       ➜ 3 | --pre               ║ $(tput sgr 0)"
  echo "$(tput setaf 6)$(tput bold)║       ➜ 4 | --pre:compress      ║ $(tput sgr 0)"
  echo "$(tput setaf 6)$(tput bold)║       ➜ 5 | --deploy            ║ $(tput sgr 0)"
  echo "$(tput setaf 6)$(tput bold)║       ➜ 6 | --dp:all            ║ $(tput sgr 0)"
  echo "$(tput setaf 6)$(tput bold)║       ➜ 7 | --dp:p              ║ $(tput sgr 0)"
  echo "$(tput setaf 6)$(tput bold)║       ➜ 8 | --dp:p2             ║ $(tput sgr 0)"
  echo "$(tput setaf 6)$(tput bold)╚═════════════════════════════════╝ $(tput sgr 0)"
  echo -e "\r"
}

header_first() {
  #  echo -e "\?"
  echo "$(tput setaf 2)$(tput bold)┌────────────────────────────────────────────────────────┐ $(tput sgr 0)"
  echo "$(tput setaf 2)$(tput bold)│ 🔥 $(tput setaf 7)Start tasks                                         $(tput setaf 2)│$(tput sgr 0)"
  echo "$(tput setaf 2)$(tput bold)└────────────────────────────────────────────────────────┘ $(tput sgr 0)"
}

header_l1() {
  echo -e "\r"
  echo "$(tput setaf 2)$(tput bold)⪢ $(tput setaf 8)[ $(tput setaf 3)Started $1$(tput setaf 8) ] $(tput setaf 5)$2 $(tput setaf 7)⪡⪢ $(tput sgr 0)"
  echo -e "\r"
}

footer_finish() {
  echo "$(tput setaf 2)$(tput bold)────────────────────────────────────────────────────────── $(tput sgr 0)"
  echo "$(tput setaf 2)$(tput bold)⪢ Total build size $(tput setaf 8) | $(tput setaf 3)$(du -hs "dist/newhope") $(tput sgr 0)"
  echo "$(tput setaf 2)$(tput bold)⪢ Total build size $(tput setaf 8) | $(tput setaf 3)$(du -hs "dist/newhope-surrogacy") $(tput sgr 0)"

  echo -e "\r"
  echo "$(tput setaf 2)$(tput bold)┌────────────────────────────────────────────────────────┐ $(tput sgr 0)"
  echo "$(tput setaf 2)$(tput bold)│  ✓ $(tput setaf 7)Done all tasks                                      $(tput setaf 2)│$(tput sgr 0)"
  echo "$(tput setaf 2)$(tput bold)└────────────────────────────────────────────────────────┘ $(tput sgr 0)"
}

deploy_ssh() {
  USER=newhopes
  HOST=usshared08.twinservers.net
  #  PORT=21098
  DIR_BUILD=$1
  DIR_REMOTE=$2
  #  rsync -arz -e --delete 'ssh -p 21098' ./dist/newhope/ newhopes@usshared08.twinservers.net:~/newsurrogacy.com/
  rsync -arz -e 'ssh -p 21098' --delete ./dist/${DIR_BUILD}/ ${USER}@${HOST}:~/${DIR_REMOTE}

  echo "$(tput setaf 2)$(tput bold)✓ $(tput setaf 7)Complete upload server $(tput setaf 2)$(tput sgr 0)"
}

clear_dist() {
  clear
  rm -rf dist
  echo "$(tput setaf 2)$(tput bold)⪢ $(tput setaf 8)[ $(tput setaf 3)Clear distributable directory$(tput setaf 8) ] $(tput sgr 0)"
}

case_command() {
  case $1 in
  --build | 1)
    clear_dist
    header_l1 "Build" "newsurrogacy.com"
    ng build

    header_l1 "Build" "newhope-surrogacy.com"
    ng build --project=newhope-surrogacy

    footer_finish
    ;;

  --ssr | 2)
    clear
    header_l1 "SSR" "newsurrogacy.com"
    ng run newhope:server

    header_l1 "SSR" "newhope-surrogacy.com"
    ng run newhope-surrogacy:server

    footer_finish
    ;;

  --pre | 3)
    clear
    header_l1 "Prerender" "newsurrogacy.com"
    ng run newhope:prerender

    header_l1 "Prerender" "newhope-surrogacy.com"
    ng run newhope-surrogacy:prerender

    footer_finish
    ;;

  --pre:compress | 4)
    header_l1 "Minified Prerender" "newsurrogacy.com | newhope-surrogacy.com"
    node scripts.js

    footer_finish
    ;;

  --deploy | 5)
    header_l1 "Deployment SSH" "newsurrogacy.com"
    deploy_ssh "newhope" "newsurrogacy.com"

    header_l1 "Deployment SSH" "newhope-surrogacy.com"
    deploy_ssh "newhope-surrogacy" "public_html"

    echo -e "\r"
    footer_finish
    ;;

  --dp:all | 6)
    clear_dist
    header_l1 "Build" "newsurrogacy.com"
    ng build
    header_l1 "Build" "newhope-surrogacy.com"
    ng build --project=newhope-surrogacy

    header_l1 "Build SSR" "newsurrogacy.com && newhope-surrogacy.com"
    ng run newhope:server
    ng run newhope-surrogacy:server

    header_l1 "Build Prerender" "newsurrogacy.com && newhope-surrogacy.com"
    ng run newhope:prerender
    ng run newhope-surrogacy:prerender

    header_l1 "Minified Prerender" "newsurrogacy.com | newhope-surrogacy.com"
    node scripts.js

    header_l1 "Deployment SSH" "newsurrogacy.com"
    deploy_ssh "newhope" "newsurrogacy.com"

    header_l1 "Deployment SSH" "newhope-surrogacy.com"
    deploy_ssh "newhope-surrogacy" "public_html"

    footer_finish
    ;;

  --dp:p | 7)
    header_l1 "Deployment SSH" "newsurrogacy.com"
    deploy_ssh "newhope" "newsurrogacy.com"

    footer_finish
    ;;

  --dp:p2 | 8)
    header_l1 "Deployment SSH" "newhope-surrogacy.com"
    deploy_ssh "newhope-surrogacy" "public_html"

    footer_finish
    ;;

  --dp:dev | 9)
    header_l1 "Deployment SSH" "dev.newsurrogacy.com"
    deploy_ssh "newhope" "dev.newsurrogacy.com"

    footer_finish
    ;;

  --rm:server | 10)
    header_l1 "Remove server folder"
    rm -rf dist/newhope-surrogacy/server
    rm -rf dist/newhope/server

    footer_finish
    ;;

  *)
    echo -n "let error commands"
    ;;
  esac
}

if [ -n "$1" ]; then
  case_command $1
else
  header
  echo -n "$(tput setaf 3)$(tput bold)➜ Enter commands: $(tput sgr 0)"
  read command

  case_command $command
fi
