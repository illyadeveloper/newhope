export const environment = {
  production: true,
  jsonUrl: '/assets/json',
  projectName: 'New Hope',
  projectSiteLink: 'newsurrogacy.com',
  projectSiteName: 'newsurrogacy',
  googleMapsApiKey: 'AIzaSyDmjyJgt2ErxUPB4wdHfJjoeAQs3feaapk',

  // for prerender
  host: 'http://localhost:4000',
  prerenderDistName: 'newhope'
};
