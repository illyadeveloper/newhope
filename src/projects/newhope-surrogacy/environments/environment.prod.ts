export const environment = {
  production: true,
  jsonUrl: '/assets/json',
  projectName: 'New Hope',
  projectSiteLink: 'newhope-surrogacy.com',
  projectSiteName: 'newhope-surrogacy',
  googleMapsApiKey: 'AIzaSyDmjyJgt2ErxUPB4wdHfJjoeAQs3feaapk',

  // for prerender
  host: 'http://localhost:4000',
  prerenderDistName: 'newhope-surrogacy'
};
