import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LayoutRootComponent} from './layout-root.component';
import {RouterModule} from "@angular/router";
import {FlexLayoutModule} from "@angular/flex-layout";
import {UiModule} from "@ui/ui.module";
import {SharedModule} from "@shared/shared.module";
import {HeaderComponent} from "@modules/layout-root/components/header/header.component";
import {FooterComponent} from "@modules/layout-root/components/footer/footer.component";

@NgModule({
  declarations: [
    LayoutRootComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FlexLayoutModule,
    UiModule,
    SharedModule
  ]
})
export class LayoutRootModule {
}
