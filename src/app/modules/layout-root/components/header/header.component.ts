import {Component, HostBinding, HostListener, Inject, PLATFORM_ID} from '@angular/core';
import {AsideService} from "@core/services/aside.service";
import {Router} from "@angular/router";
import {isPlatformBrowser} from "@angular/common";
import {NativeRefService} from "@core/refs/native-ref.service";

@Component({
  selector: 'hope-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  anime: boolean = false;
  isScroll: boolean = false;
  isBrowser = isPlatformBrowser(this.platformId);

  @HostListener('window:scroll', []) onWindowScroll() {
    (this.native.nativeDocument.documentElement.scrollTop >= 5)
      ? (this.isScroll = true)
      : (this.isScroll = false)
  }

  @HostBinding('class.--shadow') get onScroll() {
    return (this.isScroll || this.router.url.includes('contact'));
  }

  constructor(
    @Inject(PLATFORM_ID) private platformId: object,
    private native: NativeRefService,
    private router: Router,
    private asides: AsideService
  ) {
  }

  get isNotHome() {
    return (this.router.url == '/')
  }

  isMenuOpen: boolean = false;

  triggerMenu(): void {
    this.isMenuOpen = !this.isMenuOpen;

    return (this.isMenuOpen
        ? this.asides.scrollHide()
        : this.asides.scrollReset()
    )
  }

  resetPosition() {
    if (this.isBrowser) {
      setTimeout(() => {
        this.native.nativeWindow.scrollTo(0, 0);
      }, 1);
    }

    this.isMenuOpen = false;
    this.asides.scrollReset();
  }

  resetPositionHome() {
    if (this.isBrowser) {
      this.native.nativeWindow.scrollTo(0, 0);
    }

    this.isMenuOpen = false;
    this.asides.scrollReset();
  }

  openConsultationAside() {
    this.asides.consultationAsideOpen();
  }
}
