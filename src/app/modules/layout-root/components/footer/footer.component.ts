import { Component, OnInit } from '@angular/core';
import {AsideService} from "@core/services/aside.service";
import {contactsStore} from "@core/stores/contacts.store";

@Component({
  selector: 'hope-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  phone: string = '';
  phoneLink: string = '';

  email: string = '';
  address: string = '';
  addressLink: string = '';

  facebook: string = '';
  linkedin: string = '';
  instagram: string = '';

  watsapp: string ='';

  constructor(
    private asides: AsideService
  ) { }

  ngOnInit(): void {
    this.phone = contactsStore.PHONE;
    this.phoneLink = contactsStore.PHONE_LINK
    this.address = contactsStore.ADDRESS;
    this.addressLink = contactsStore.ADDRESS_LINK;
    this.email = contactsStore.EMAIL;

    this.facebook = contactsStore.SOC_FB;
    this.linkedin = contactsStore.SOC_LN;
    this.watsapp = contactsStore.SOC_WA;

    this.instagram = contactsStore.SOC_INST;
  }

  openConsultationAside() {
    this.asides.consultationAsideOpen();
  }
}
