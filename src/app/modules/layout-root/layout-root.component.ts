import {Component, OnInit, Inject, PLATFORM_ID} from '@angular/core';
import {isPlatformBrowser} from '@angular/common';
import {Router} from "@angular/router";
import {Observable} from 'rxjs';
import {AsideService} from "@core/services/aside.service";
import {CookieConsentService} from "@core/services/cookie-consent.service";
import {EFFECT_SHOW_UP, EFFECT_BLOCK_ON_DOWN} from "@core/animation/animation.effects";
import {NativeRefService} from "@core/refs/native-ref.service";

@Component({
  selector: 'hope-layout-root',
  templateUrl: './layout-root.component.html',
  styleUrls: ['./layout-root.component.scss'],
  animations: [...EFFECT_SHOW_UP, ...EFFECT_BLOCK_ON_DOWN]
})
export class LayoutRootComponent implements OnInit {
  url: string = '';

  isConsultationShow$: Observable<boolean> = this.asides.isConsultationShow$.asObservable();
  triggerCookieWindow$: Observable<boolean> = this.cookieConsent.triggerCookieWindow$.asObservable();

  isBrowser = isPlatformBrowser(this.platformId);
  isHeaderAnime: boolean = false;

  constructor(
    @Inject(PLATFORM_ID) private platformId: object,
    private router: Router,
    private native: NativeRefService,
    private asides: AsideService,
    private cookieConsent: CookieConsentService,
  ) {
  }

  get isContact(): boolean {
    return (
      this.router.url.includes('contact')
    );
  }

  ngOnInit(): void {
    this.url = this.router.url.split('?')[0];

    if (this.isBrowser) {
      this.checkBrowser();
    }

    // setTimeout(() => {
    //   this.isHeaderAnime = true;
    // }, 1300)


  }

  hideConsultationAside() {
    this.asides.consultationAsideHide();
    return false;
  }

  checkBrowser() {
    this.native.nativeWindow.navigator.appVersion.indexOf('Win') != -1
      ? this.native.html.classList.add('--small-scroll')
      : this.native.html.classList.remove('--small-scroll');
  }
}
