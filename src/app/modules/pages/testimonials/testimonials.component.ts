import {Component, OnInit, OnDestroy} from '@angular/core';
import {REVIEWS} from '@core/stores/testimonils.store'
import {EFFECT_SCALE} from "@core/animation/animation.effects";
import {seoTestimonialsPage} from "@core/stores/seo.store";
import {MetaService} from "@core/services/meta.service";

@Component({
  selector: 'hope-testimonials',
  templateUrl: './testimonials.component.html',
  styleUrls: ['./testimonials.component.scss'],
  animations: [...EFFECT_SCALE]
})
export class TestimonialsComponent implements OnInit, OnDestroy {

  list: any = [];
  isAnimation: boolean = false

  constructor(
    protected metaService: MetaService
  ) {
    this.metaService.createCanonicalURL();
    this.metaService.updateMeta(seoTestimonialsPage, true);

    this.list = REVIEWS
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.isAnimation = true;
    }, 0);
  }

  ngOnDestroy() {
    this.isAnimation = false;
  }
}
