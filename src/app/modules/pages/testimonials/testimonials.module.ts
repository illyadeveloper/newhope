import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {TestimonialsRoutingModule} from './testimonials-routing.module';
import {TestimonialsComponent} from './testimonials.component';
import {FlexLayoutModule} from "@angular/flex-layout";
import {UiModule} from "@ui/ui.module";

@NgModule({
  declarations: [
    TestimonialsComponent
  ],
  imports: [
    CommonModule,
    TestimonialsRoutingModule,
    FlexLayoutModule,
    UiModule
  ]
})
export class TestimonialsModule {
}
