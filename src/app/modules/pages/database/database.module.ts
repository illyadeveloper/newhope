import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DatabaseRoutingModule } from './database-routing.module';
import { DatabaseComponent } from './database.component';
import { CandidateCardComponent } from './components/candidate-card/candidate-card.component';
import { FormAccessDatabaseComponent } from './components/form-access-database/form-access-database.component';
import {UiModule} from "@ui/ui.module";
import {FlexLayoutModule} from "@angular/flex-layout";


@NgModule({
  declarations: [
    DatabaseComponent,
    CandidateCardComponent,
    FormAccessDatabaseComponent
  ],
  imports: [
    CommonModule,
    DatabaseRoutingModule,
    UiModule,
    FlexLayoutModule,
  ]
})
export class DatabaseModule { }
