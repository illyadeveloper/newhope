import {Component, AfterViewInit, OnDestroy, Inject, PLATFORM_ID, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {BlogService} from "@core/services/blog.service";
import {isPlatformBrowser} from "@angular/common";
import {MetaService} from "@core/services/meta.service";
import {seoBlogPage} from "@core/stores/seo.store";
import {ClearStrPipe} from "@core/pipes/clear-str.pipe";


@Component({
  selector: 'hope-blog-detail',
  templateUrl: './blog-detail.component.html',
  styleUrls: ['./blog-detail.component.scss']
})
export class BlogDetailComponent implements OnDestroy {
  name: string = '';
  img: string = '';
  content: string = '';
  time: string = '';

  isBrowser = isPlatformBrowser(this.platformId);
  isLoad: boolean = false;

  private subBlog: any;

  constructor(
    @Inject(PLATFORM_ID) private platformId: object,
    private route: ActivatedRoute,
    private rout: Router,
    private blogService: BlogService,
    protected metaService: MetaService,
  ) {
    this.blogService.getBlogData().then((res) => this.setPostDetail(res));
    this.metaService.createCanonicalURL();
    // this.metaService.removeMeta();
    this.isLoad = true;
    // if (this.isBrowser) {
    //
    //
    // }
    // this.rout.events.subscribe((evn) => {
    //     if (this.route.url != this.route.params['id']) {
    //       console.log('this.route.url =', this.route.url , ' this.route.params ', this.route.params['id'])
    //       return this.onBeckToBlog();
    //     }
    //   }
    // );
  }

  // ngOnInit(): void {
  //   // if (this.postData.name != this.route.params.subscribe(r => r['id'])) {
  //   //   return this.onBeckToBlog();
  //   // }
  // }

  setMetaData(data?: any): any {
    let meta: object  = {
      TITLE: data.name,
      KEYWORDS: seoBlogPage.KEYWORDS,
      DESCRIPTION: data.content.slice(3, 150),
      IMG: 'blog/' + data.poster
    };

    return meta;
  }

  setPostDetail(data?: any): any {
    // this.postData = data;
    if (data === null || data === undefined || data.length === 0) {
      return this.onBeckToBlog();
    }

    this.subBlog = this.route.params.subscribe(
      (param) => {
        let url = new ClearStrPipe().transform(param['id']);
        let post: any = [];

        post = data.find((item: any) => (new ClearStrPipe().transform(item.name) === url))

        this.metaService.updateMeta(this.setMetaData(post), true);

        this.name = post.name;
        this.time = post.date
        this.img = post.poster;
        this.content = post.content;
      }
    )
  }

  onBeckToBlog() {
    return this.rout.navigateByUrl('blog')
  }

  ngOnDestroy(): void {
    if (this.isBrowser) {
      this.subBlog.unsubscribe();
    }
  }
}
