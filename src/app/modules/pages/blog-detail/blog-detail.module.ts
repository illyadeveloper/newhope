import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {BlogDetailRoutingModule} from './blog-detail-routing.module';
import {BlogDetailComponent} from "./blog-detail.component";
import {FlexLayoutModule} from "@angular/flex-layout";
import {RouterModule} from "@angular/router";

@NgModule({
  declarations: [BlogDetailComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    BlogDetailRoutingModule,
    RouterModule
  ]
})
export class BlogDetailModule {
}
