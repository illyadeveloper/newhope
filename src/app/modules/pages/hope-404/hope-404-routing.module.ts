import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {Hope404Module} from "./hope-404.module";
import {HopeNotComponent} from "./hope-not/hope-not.component";

const routes: Routes = [
  {
    path: '',
    component: HopeNotComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Hope404RoutingModule {
}
