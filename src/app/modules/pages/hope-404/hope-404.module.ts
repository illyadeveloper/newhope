import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Hope404RoutingModule } from './hope-404-routing.module';
import { HopeNotComponent } from './hope-not/hope-not.component';
import {UiModule} from "@ui/ui.module";


@NgModule({
  declarations: [
    HopeNotComponent
  ],
  imports: [
    CommonModule,
    Hope404RoutingModule,
    UiModule
  ]
})
export class Hope404Module { }
