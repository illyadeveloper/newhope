import {Component, OnInit, Inject, PLATFORM_ID, Optional} from '@angular/core';
import {RESPONSE} from '@nguniversal/express-engine/tokens';
import {seo404Page} from "@core/stores/seo.store";
import {isPlatformBrowser} from "@angular/common";
import {MetaService} from "@core/services/meta.service";

@Component({
  selector: 'hope-hope-not',
  templateUrl: './hope-not.component.html',
  styleUrls: ['./hope-not.component.scss']
})
export class HopeNotComponent implements OnInit {
  isBrowser = isPlatformBrowser(this.platformId);

  constructor(
    @Optional() @Inject(RESPONSE) private response: any,
    @Inject(PLATFORM_ID) private platformId: object,
    protected metaService: MetaService) {
  }

  ngOnInit(): void {
    this.metaService.createCanonicalURL();
    this.metaService.updateMeta(seo404Page, false);

    if (this.isBrowser || !!this.response) {
      // this.response.status(404);
      this.response.statusCode = 404;
      this.response.statusMessage = '404 - Page Not Found';
    }

    // if (!!this.response) {
    //   this.response.statusCode = 404;
    //   this.response.statusMessage = '404 - Page Not Found';
    // }
  }
}
