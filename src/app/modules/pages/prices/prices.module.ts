import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PricesRoutingModule} from './prices-routing.module';
import {PricesComponent} from './prices.component';
import {UiModule} from "../../../ui/ui.module";
import {FlexLayoutModule} from "@angular/flex-layout";
import { ProgramItemComponent } from './components/program-item/program-item.component';

@NgModule({
  declarations: [
    PricesComponent,
    ProgramItemComponent
  ],
  imports: [
    CommonModule,
    PricesRoutingModule,
    UiModule,
    FlexLayoutModule
  ]
})
export class PricesModule {
}
