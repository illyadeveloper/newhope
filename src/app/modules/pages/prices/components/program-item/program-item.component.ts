import {Component, Input} from '@angular/core';
import {AsideService} from "@core/services/aside.service";

@Component({
  selector: 'hope-program-item',
  templateUrl: './program-item.component.html',
  styleUrls: ['./program-item.component.scss']
})
export class ProgramItemComponent {

  @Input() name: string = '';
  @Input() price: string = '';
  @Input() description: string = '';
  @Input() icon: string = '';
  @Input() detailsList: any = [];

  detailList: boolean = false;

  constructor(private asides: AsideService) {
  }

  showDetailList() {
    this.detailList = !this.detailList;
  }

  openConsultationAside() {
    this.asides.consultationAsideOpen();
  }
}
