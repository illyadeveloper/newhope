import {Component} from '@angular/core';
import {seoPricesPage} from "@core/stores/seo.store";
import {MetaService} from "@core/services/meta.service";
import {PROGRAMS_LIST} from "@core/stores/programs.store"

@Component({
  selector: 'hope-prices',
  templateUrl: './prices.component.html',
  styleUrls: ['./prices.component.scss']
})
export class PricesComponent {

  list: any = [];

  constructor(
    private metaService: MetaService
  ) {
    this.metaService.createCanonicalURL();
    this.metaService.updateMeta(seoPricesPage, true);
    this.metaService.updateFeedsURL('_costs');

    this.list = PROGRAMS_LIST;
  }
}
