import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SurrogacyProgramsComponent} from "./surrogacy-programs.component";

const routes: Routes = [
  {
    path: '',
    component: SurrogacyProgramsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SurrogacyProgramsRoutingModule {
}
