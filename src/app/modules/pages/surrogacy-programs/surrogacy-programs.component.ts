import {Component} from '@angular/core';
import {seoSurrogacyProgramsPage} from "@core/stores/seo.store";
import {MetaService} from "@core/services/meta.service";

@Component({
  selector: 'hope-surrogacy-programs',
  templateUrl: './surrogacy-programs.component.html',
  styleUrls: ['./surrogacy-programs.component.scss']
})
export class SurrogacyProgramsComponent {
  constructor(
    private metaService: MetaService
  ) {
    this.metaService.createCanonicalURL();
    this.metaService.updateMeta(seoSurrogacyProgramsPage, true);
  }
}
