import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SurrogacyProgramsRoutingModule } from './surrogacy-programs-routing.module';
import { SurrogacyProgramsComponent } from './surrogacy-programs.component';
import {FlexLayoutModule} from "@angular/flex-layout";
import {UiModule} from "../../../ui/ui.module";


@NgModule({
  declarations: [
    SurrogacyProgramsComponent
  ],
  imports: [
    CommonModule,
    SurrogacyProgramsRoutingModule,
    FlexLayoutModule,
    UiModule
  ]
})
export class SurrogacyProgramsModule { }
