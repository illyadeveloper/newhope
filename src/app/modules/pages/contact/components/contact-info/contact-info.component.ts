import {Component, OnInit} from '@angular/core';
import {contactsStore} from "@core/stores/contacts.store";
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'hope-contact-info',
  templateUrl: './contact-info.component.html',
  styleUrls: ['./contact-info.component.scss']
})
export class ContactInfoComponent implements OnInit {

  toggleContent: boolean = false;

  phone: string = '';
  phoneLink: string = '';
  // phoneSecond: string = '';
  // phoneSecondLink: string = '';

  email: string = '';
  address: string = '';
  addressLink: string = '';

  facebook: string = '';
  linkedin: string = '';
  instagram: string = '';

  viber: string = '';
  telegram: string = '';
  watsapp: string = '';

  constructor(private sanitizer: DomSanitizer) {
  }

  ngOnInit(): void {
    this.phone = contactsStore.PHONE;
    this.phoneLink = contactsStore.PHONE_LINK
    // this.phoneSecond = contactsStore.PHONE_SECOND;
    // this.phoneSecondLink = contactsStore.PHONE_SECOND_LINK
    this.address = contactsStore.ADDRESS;
    this.addressLink = contactsStore.ADDRESS_LINK;
    this.email = contactsStore.EMAIL;

    this.facebook = contactsStore.SOC_FB;
    this.linkedin = contactsStore.SOC_LN;
    this.instagram = contactsStore.SOC_INST;
    this.viber = contactsStore.SOC_VB;
    this.telegram = contactsStore.SOC_TG;
    this.watsapp = contactsStore.SOC_WA;
  }

  showContent() {
    this.toggleContent = !this.toggleContent;
  }

  sanitize(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }
}
