import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContactRoutingModule } from './contact-routing.module';
import { ContactComponent } from './contact.component';
import { ContactInfoComponent } from './components/contact-info/contact-info.component';
import {GoogleMapsModule} from "@angular/google-maps";
import {FlexLayoutModule} from "@angular/flex-layout";
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';

@NgModule({
  declarations: [
    ContactComponent,
    ContactInfoComponent,
  ],
  imports: [
    CommonModule,
    ContactRoutingModule,
    FlexLayoutModule,
    GoogleMapsModule,
    HttpClientModule,
    HttpClientJsonpModule
  ]
})
export class ContactModule { }
