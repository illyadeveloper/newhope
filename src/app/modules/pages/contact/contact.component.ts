import {Component, Inject, InjectionToken, PLATFORM_ID} from '@angular/core';
import {environment} from "@environments/environment";
import {seoContactPage} from "@core/stores/seo.store";
import {contactsStore} from "@core/stores/contacts.store";
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

import {isPlatformBrowser} from '@angular/common';
import {NativeRefService} from "@core/refs/native-ref.service";
import {MetaService} from "@core/services/meta.service";
import {MAP_OPTION, MAP_MARKER_OPTIONS} from "@core/stores/location.store";

@Component({
  selector: 'hope-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
})
export class ContactComponent {
  apiLoaded: Observable<boolean>;

  isBrowser = isPlatformBrowser(this.platformId);

  position: any = {lat: contactsStore.POSITION_LAT, lng: contactsStore.POSITION_LNG};
  map: any = MAP_OPTION;
  marker: any = MAP_MARKER_OPTIONS;

  zoom = 12;
  center: google.maps.LatLngLiteral = this.position;
  options: google.maps.MapOptions = this.map
  optionsMarker: google.maps.MarkerOptions = this.marker;

  constructor(
    @Inject(PLATFORM_ID) private platformId: object,
    private native: NativeRefService,
    private metaService: MetaService,
    private  httpClient: HttpClient) {

    this.apiLoaded = httpClient.jsonp(`https://maps.googleapis.com/maps/api/js?key=${environment.googleMapsApiKey}`, 'callback')
      .pipe(
        map(() => true),
        catchError(() => of(false)),
      );
  }

  ngOnInit(): void {
    this.metaService.createCanonicalURL();
    this.metaService.updateMeta(seoContactPage, true);

    if (this.isBrowser) {
      this.native.nativeWindow.navigator.geolocation.getCurrentPosition((position: any) => {
        this.center.lat = position.coords.latitude;
      })
    }
  }
}


