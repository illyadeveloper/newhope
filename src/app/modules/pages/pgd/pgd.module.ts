import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PgdRoutingModule } from './pgd-routing.module';
import { PgdComponent } from './pgd.component';
import {FlexLayoutModule} from "@angular/flex-layout";
import {UiModule} from "../../../ui/ui.module";
import {RouterModule} from "@angular/router";


@NgModule({
  declarations: [
    PgdComponent
  ],
  imports: [
    CommonModule,
    PgdRoutingModule,
    FlexLayoutModule,
    RouterModule,
    UiModule
  ]
})
export class PgdModule { }
