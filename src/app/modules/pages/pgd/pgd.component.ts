import {Component} from '@angular/core';
import {seoPgdPage} from "@core/stores/seo.store";
import {MetaService} from "@core/services/meta.service";

@Component({
  selector: 'hope-pgd',
  templateUrl: './pgd.component.html',
  styleUrls: ['./pgd.component.scss']
})
export class PgdComponent {

  constructor(
    private metaService: MetaService
  ) {
    this.metaService.createCanonicalURL();
    this.metaService.updateMeta(seoPgdPage, true);
  }
}
