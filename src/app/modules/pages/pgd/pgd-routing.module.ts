import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PgdComponent} from "./pgd.component";

const routes: Routes = [
  {
    path:'',
    component: PgdComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PgdRoutingModule { }
