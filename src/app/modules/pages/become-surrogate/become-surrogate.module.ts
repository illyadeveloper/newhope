import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BecomeSurrogateRoutingModule } from './become-surrogate-routing.module';
import { BecomeSurrogateComponent } from './become-surrogate.component';

@NgModule({
  declarations: [
    BecomeSurrogateComponent
  ],
  imports: [
    CommonModule,
    BecomeSurrogateRoutingModule
  ]
})
export class BecomeSurrogateModule { }
