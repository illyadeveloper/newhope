import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {BecomeSurrogateComponent} from "@modules/pages/become-surrogate/become-surrogate.component";

const routes: Routes = [
  {
    path: '',
    component: BecomeSurrogateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BecomeSurrogateRoutingModule { }
