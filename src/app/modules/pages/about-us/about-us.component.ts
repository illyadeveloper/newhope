import {Component} from '@angular/core';
import {MetaService} from "@core/services/meta.service";
import {seoAboutUsPage} from "@core/stores/seo.store";

@Component({
  selector: 'hope-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent {

  constructor(
    private metaService: MetaService
  ) {
    this.metaService.createCanonicalURL();
    this.metaService.updateMeta(seoAboutUsPage, true);
  }
}
