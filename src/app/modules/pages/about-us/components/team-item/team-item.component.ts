import {Component, Input} from '@angular/core';

@Component({
  selector: 'hope-team-item',
  templateUrl: './team-item.component.html',
  styleUrls: ['./team-item.component.scss']
})
export class TeamItemComponent {

  @Input() image: string = '';
  @Input() name: string = '';
  @Input() position: string = '';
  @Input() socFb: string = '';
  @Input() socIn: string = '';
  @Input() socIns: string = '';

  constructor() {
  }
}
