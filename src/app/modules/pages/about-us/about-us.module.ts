import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AboutUsRoutingModule } from './about-us-routing.module';
import { AboutUsComponent } from './about-us.component';
import { TeamItemComponent } from './components/team-item/team-item.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { TeamComponent } from './components/team/team.component';
import {UiModule} from "../../../ui/ui.module";
import {FlexLayoutModule} from "@angular/flex-layout";

@NgModule({
  declarations: [
    AboutUsComponent,
    TeamItemComponent,
    WelcomeComponent,
    TeamComponent
  ],
  imports: [
    CommonModule,
    AboutUsRoutingModule,
    UiModule,
    FlexLayoutModule
  ]
})
export class AboutUsModule { }
