import {Component} from '@angular/core';
import {MetaService} from "@core/services/meta.service";
import {seoFaqPage} from "@core/stores/seo.store";

@Component({
  selector: 'hope-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent {

  constructor(
    private metaService: MetaService,
  ) {
    this.metaService.createCanonicalURL();
    this.metaService.updateMeta(seoFaqPage, true);
  }
}
