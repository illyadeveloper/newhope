import {Component, Input, HostBinding, HostListener} from '@angular/core';

@Component({
  selector: 'hope-faq-item',
  templateUrl: './faq-item.component.html',
  styleUrls: ['./faq-item.component.scss']
})
export class FaqItemComponent {
  @Input() caption: string = '';

  trigger: boolean = false;

  @HostBinding('class.--is-active') get isActive() {
    return this.trigger;
  };

  @HostListener('click', ['$event.target']) onClick() {
    setTimeout(() => {
      this.trigger = !this.trigger;
    }, 300)
  }

  constructor() {
  }
}
