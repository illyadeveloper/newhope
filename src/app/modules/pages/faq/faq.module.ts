import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FaqRoutingModule} from './faq-routing.module';
import {FaqComponent} from './faq.component';
import {UiModule} from "../../../ui/ui.module";
import {FaqItemComponent} from './components/faq-item/faq-item.component';
import {FlexLayoutModule} from "@angular/flex-layout";


@NgModule({
  declarations: [
    FaqComponent,
    FaqItemComponent
  ],
  imports: [
    CommonModule,
    FaqRoutingModule,
    UiModule,
    FlexLayoutModule
  ]
})
export class FaqModule {
}
