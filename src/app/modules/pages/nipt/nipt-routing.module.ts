import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {NiptModule} from "./nipt.module";
import {NiptComponent} from "./nipt.component";

const routes: Routes = [
  {
    path: '',
    component: NiptComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NiptRoutingModule { }
