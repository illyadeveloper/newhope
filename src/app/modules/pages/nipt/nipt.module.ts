import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NiptRoutingModule } from './nipt-routing.module';
import { NiptComponent } from './nipt.component';
import {FlexLayoutModule} from "@angular/flex-layout";
import {UiModule} from "../../../ui/ui.module";


@NgModule({
  declarations: [
    NiptComponent
  ],
  imports: [
    CommonModule,
    NiptRoutingModule,
    FlexLayoutModule,
    UiModule
  ]
})
export class NiptModule { }
