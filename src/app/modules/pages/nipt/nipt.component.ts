import {Component} from '@angular/core';
import {seoNiptPage} from "@core/stores/seo.store";
import {MetaService} from "@core/services/meta.service";

@Component({
  selector: 'hope-tionnipt',
  templateUrl: './nipt.component.html',
  styleUrls: ['./nipt.component.scss']
})
export class NiptComponent {

  constructor(
    private metaService: MetaService
  ) {
    this.metaService.createCanonicalURL();
    this.metaService.updateMeta(seoNiptPage, true);
  }
}
