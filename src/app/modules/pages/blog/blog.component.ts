import {Component, Inject, OnInit, PLATFORM_ID} from '@angular/core';
import {Router, ActivatedRoute} from "@angular/router";
import {seoBlogPage} from "@core/stores/seo.store";
import {BlogService} from "@core/services/blog.service";
import {MetaService} from "@core/services/meta.service";
import {ClearStrPipe} from "@core/pipes/clear-str.pipe";
import {isPlatformBrowser} from "@angular/common";

@Component({
  selector: 'hope-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {

  blogData: any = [];

  // blogData$: BehaviorSubject<any> = new BehaviorSubject<any>([]);

  // isBrowser = isPlatformBrowser(this.platformId);

  constructor(
    // @Inject(PLATFORM_ID) private platformId: object,
    private router: Router,
    private blogService: BlogService,
    private metaService: MetaService,
    protected activeRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.metaService.createCanonicalURL();
    this.metaService.updateMeta(seoBlogPage, true);
    this.metaService.updateFeedsURL('_blog');

    this.blogService.getBlogData().then(res => this.blogData = res);
    //
    // if (this.isBrowser) {

    //  this.blogData$.next(this.blogData)
    // this.blogService.getBlogData().then(res => this.blogData.next(res));
    // }

    // console.log(
    //   data.sort((a, b) => ref[a.Id] - ref[b.Id])
    // )
  }

  //
  // get sortByLastModifiedDesc() {
  //   return this.blogData.sort((a: any, b: any) => {
  //     return <any>new Date(b.date) - <any>new Date(a.date);
  //   });
  // }
  // get sortByLastModifiedAsend() {
  //   return this.blogData.sort((a: any, b: any) => {
  //     return <any>new Date(b.date) - <any>new Date(a.date);
  //   });
  // }


  sliceContent(str: string): string {
    str = str.slice(0, 150).split('<p>').join('');
    // console.log('str.slice(0, 100),', str);
    return str;
  }

  trackByFn(index?: number, item?: any) {
    return item.id;
  }

  onViewDetails(id?: any, name?: any) {
    let post = new ClearStrPipe().transform(name);

    this.router.navigate(
      ['blog/', post]
    );
  }
}

