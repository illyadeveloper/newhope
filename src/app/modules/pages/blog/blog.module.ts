import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {BlogRoutingModule} from './blog-routing.module';
import {BlogComponent} from './blog.component';
import {CardComponent} from './components/card/card.component';
import {FlexLayoutModule} from "@angular/flex-layout";
import {RouterModule} from "@angular/router";

// import { BlogDetailComponent } from './blog-detail/blog-detail.component';

@NgModule({
  declarations: [
    BlogComponent,
    CardComponent
    // BlogDetailComponent
  ],
  imports: [
    CommonModule,
    BlogRoutingModule,
    FlexLayoutModule,
    RouterModule
  ]
})
export class BlogModule {
}
