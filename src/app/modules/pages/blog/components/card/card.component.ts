import {Component, Input} from '@angular/core';

@Component({
  selector: 'hope-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent {

  @Input() img: string = '';
  @Input() name: string = '';
  @Input() description: string = '';
  @Input() date: string = '';
}
