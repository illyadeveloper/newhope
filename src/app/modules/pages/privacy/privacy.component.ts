import {Component, OnInit, AfterViewInit, OnDestroy, ElementRef, Inject, PLATFORM_ID} from '@angular/core';
import {ActivatedRoute, Router,} from '@angular/router';
import {Subscription} from 'rxjs';
import {seoPrivacyPage} from "@core/stores/seo.store";
import {environment} from '@environments/environment';
import {MetaService} from "@core/services/meta.service";
import {NativeRefService} from "@core/refs/native-ref.service";
import {contactsStore} from "@core/stores/contacts.store";
import {AsideService} from "@core/services/aside.service";
import {isPlatformBrowser} from "@angular/common";

const PRIVACY_NAV = [
  {
    hash: 's1',
    caption: 'How we use your personal information'
  },
  {
    hash: 's2',
    caption: 'Privacy policies'
  },
  {
    hash: 's3',
    caption: 'Other Sites and Services'
  },
  {
    hash: 's4',
    caption: 'Our Privacy Promise'
  },
  {
    hash: 's5',
    caption: 'Why do we collect and use your data?'
  },
  {
    hash: 's6',
    caption: 'How do we collect and create your data?'
  },
  {
    hash: 's7',
    caption: 'How long do we keep your data?'
  },
  {
    hash: 's8',
    caption: 'How to get a copy of your data'
  },
  {
    hash: 's9',
    caption: 'Children\'s information'
  },
  {
    hash: 's10',
    caption: 'Online privacy policy only'
  },
  {
    hash: 's11',
    caption: 'Security Practices'
  },
  {
    hash: 's12',
    caption: 'Cookies'
  },
  {
    hash: 's13',
    caption: 'Log files'
  },
  {
    hash: 's14',
    caption: 'International Data Transfers'
  },
  {
    hash: 's15',
    caption: 'Changes to this Privacy Policy'
  },
  {
    hash: 's16',
    caption: 'How to Contact Us'
  },
  // {
  //   hash: 's12',
  //   caption: ''
  // }
];

@Component({
  selector: 'hope-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.scss']
})
export class PrivacyComponent implements OnInit, AfterViewInit, OnDestroy {
  companyName: string = environment.projectName + '™'; //'®''™'
  siteLink: string = environment.projectSiteLink;
  siteName: string = environment.projectSiteName;
  contactAddress: string = contactsStore.ADDRESS + ', ' + contactsStore.ADDRESS_INDEX;
  contactMail: string = contactsStore.EMAIL;
  contactPhones: string = contactsStore.PHONE;

  isBrowser = isPlatformBrowser(this.platformId);

  list: any = PRIVACY_NAV;

  constructor(
    @Inject(PLATFORM_ID) private platformId: object,
    private route: ActivatedRoute,
    private routes: Router,
    private metaService: MetaService,
    private native: NativeRefService,
    private asides: AsideService
  ) {
    this.metaService.createCanonicalURL();
    this.metaService.updateMeta(seoPrivacyPage, true);

    // this.routes.navigateByUrl('/privacy-policy', {
    //   state: {ignoreLoadingBar: true},
    // }).then(r =>{
    //   return r
    // });

  }

  private fragment: string | null;
  fragSub: Subscription;

  ngOnInit(): void {
    if (this.isBrowser) {
      this.fragSub = this.route.fragment.subscribe(fragment => {
        this.fragment = fragment;
      });
    }
  }

  ngAfterViewInit(): void {
    try {
      if (this.isBrowser) {
        this.goToSection(this.fragment);
      }
    } catch (e) {
    }
  }

  ngOnDestroy() {
    if (this.isBrowser) {
      this.fragSub.unsubscribe();
    }
  }

  goToSection(id) {
    const offset = -116;
    let element = this.native.nativeDocument.querySelector('#' + `${id}`);
    const posTop = element?.getBoundingClientRect().top + this.native.nativeWindow.pageYOffset + offset;

    this.routes.navigate([], {fragment: id});

    setTimeout(() => {
      (id != 's1')
        ? this.native.nativeWindow.scrollTo({top: posTop, behavior: 'smooth'})
        : this.native.nativeWindow.scrollTo(0, 0)
    }, 100);
  }

  navLinkSetActive(name): boolean {
    if (name === this.list[0].hash && this.fragment == null) {
      return true;
    } else {
      return name === this.fragment;
    }
  }

  openConsultationAside() {
    this.asides.consultationAsideOpen();
  }
}
