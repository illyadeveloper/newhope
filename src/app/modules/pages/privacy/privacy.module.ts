import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PrivacyRoutingModule} from './privacy-routing.module';
import {PrivacyComponent} from './privacy.component';
import {UiModule} from "@ui/ui.module";
import {FlexLayoutModule} from "@angular/flex-layout";
import {RouterModule} from "@angular/router";


@NgModule({
  declarations: [
    PrivacyComponent
  ],
  imports: [
    CommonModule,
    PrivacyRoutingModule,
    FlexLayoutModule,
    RouterModule,
    UiModule
  ]
})
export class PrivacyModule {
}
