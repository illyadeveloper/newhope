import {Component} from '@angular/core';
import {seoNgsPage} from "@core/stores/seo.store";
import {MetaService} from "@core/services/meta.service";

@Component({
  selector: 'hope-ngs24',
  templateUrl: './ngs24.component.html',
  styleUrls: ['./ngs24.component.scss']
})
export class Ngs24Component {

  constructor(
    protected metaService: MetaService
  ) {
    this.metaService.createCanonicalURL();
    this.metaService.updateMeta(seoNgsPage, true);
  }
}
