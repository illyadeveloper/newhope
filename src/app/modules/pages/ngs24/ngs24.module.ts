import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Ngs24RoutingModule } from './ngs24-routing.module';
import { Ngs24Component } from './ngs24.component';
import {FlexLayoutModule} from "@angular/flex-layout";
import {UiModule} from "../../../ui/ui.module";
import {RouterModule} from "@angular/router";


@NgModule({
  declarations: [
    Ngs24Component
  ],
  imports: [
    CommonModule,
    Ngs24RoutingModule,
    FlexLayoutModule,
    RouterModule,
    UiModule
  ]
})
export class Ngs24Module { }
