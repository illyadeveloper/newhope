import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {Ngs24Component} from "./ngs24.component";

const routes: Routes = [
  {
    path: '',
    component: Ngs24Component
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Ngs24RoutingModule {
}
