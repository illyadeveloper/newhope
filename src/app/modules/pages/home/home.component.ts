import {Component} from '@angular/core';
import {seoHomePage} from "@core/stores/seo.store";
import {MetaService} from "@core/services/meta.service";

@Component({
  selector: 'hope-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  constructor(
    private metaService: MetaService
  ) {
    this.metaService.createCanonicalURL();
    this.metaService.updateMeta(seoHomePage, true);
    this.metaService.updateFeedsURL('index');
  }
}
