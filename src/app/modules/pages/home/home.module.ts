import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {HomeRoutingModule} from './home-routing.module';
import {HomeComponent} from './home.component';
import {FirstSectionComponent} from './components/first-section/first-section.component';
import {ServicesComponent} from './components/services/services.component';
import {GeneticComponent} from './components/genetic/genetic.component';
import {EffortsComponent} from './components/efforts/efforts.component';
import {InfoComponent} from './components/info/info.component';
import {RewiewsComponent} from './components/rewiews/rewiews.component';
import {UiModule} from "@ui/ui.module";
import {BenefitsComponent} from "./components/benefits/benefits.component";
import {SurrogacyComponent} from "./components/surrogacy/surrogacy.component";
import {FlexLayoutModule} from "@angular/flex-layout";
import {SwiperModule} from 'swiper/angular';
import {ExaminationComponent} from './components/examination/examination.component';
import {PartnerComponent} from './components/partner/partner.component';

@NgModule({
  declarations: [
    HomeComponent,
    FirstSectionComponent,
    ServicesComponent,
    GeneticComponent,
    EffortsComponent,
    InfoComponent,
    RewiewsComponent,
    BenefitsComponent,
    SurrogacyComponent,
    ExaminationComponent,
    PartnerComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    UiModule,
    FlexLayoutModule,
    SwiperModule
  ]
})
export class HomeModule {
}
