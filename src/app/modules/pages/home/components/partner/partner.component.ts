import {Component, ElementRef, HostListener, ViewChild} from '@angular/core';
import {EFFECT_BLOCK_ON_TOP} from "@core/animation/animation.effects";
import {windowScrollService} from "@core/services/window-scroll.service";

@Component({
  selector: 'hope-partner',
  templateUrl: './partner.component.html',
  styleUrls: ['./partner.component.scss'],
  animations: [...EFFECT_BLOCK_ON_TOP]
})
export class PartnerComponent extends windowScrollService {
  @ViewChild('parentHostElm') parentHostElm: any = ElementRef;

  @HostListener('window:scroll', []) onWindowScroll() {
    this.isScrolledIntoView(this.parentHostElm);
  }

  constructor() {
    super()
  }
}
