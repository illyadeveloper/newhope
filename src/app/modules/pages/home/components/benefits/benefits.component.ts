import {Component, ElementRef, HostListener, ViewChild} from '@angular/core';
import {windowScrollService} from "@core/services/window-scroll.service";
import {EFFECT_TEXT_ON_TOP, EFFECT_SCALE, EFFECT_BLOCKS_ON_DOWN} from "@core/animation/animation.effects";

@Component({
  selector: 'hope-benefits',
  templateUrl: './benefits.component.html',
  styleUrls: ['./benefits.component.scss'],
  animations: [...EFFECT_TEXT_ON_TOP, ...EFFECT_SCALE, ...EFFECT_BLOCKS_ON_DOWN]
})
export class BenefitsComponent extends windowScrollService {

  @ViewChild('benefitsHostElm') benefitsHostElm: any = ElementRef;

  @HostListener('window:scroll', []) onWindowScroll() {
    this.isScrolledIntoView(this.benefitsHostElm);
  }

  constructor() {
    super()
  }
}
