import {Component, ElementRef, HostListener, ViewChild} from '@angular/core';
import {EFFECT_SLIDE_ON_RIGhT, EFFECT_TEXT_ON_LEFT_TO_RIGHT} from "@core/animation/animation.effects";
import {windowScrollService} from "@core/services/window-scroll.service";

@Component({
  selector: 'hope-efforts',
  templateUrl: './efforts.component.html',
  styleUrls: ['./efforts.component.scss'],
  animations: [...EFFECT_SLIDE_ON_RIGhT, ...EFFECT_TEXT_ON_LEFT_TO_RIGHT]
})
export class EffortsComponent extends windowScrollService {

  @ViewChild('effortsHostElm') effortsHostElm: any = ElementRef;

  @HostListener('window:scroll', []) onWindowScroll() {
    this.isScrolledIntoView(this.effortsHostElm);
  }

  constructor() {
    super()
  }
}
