import {Component, ElementRef, HostListener, ViewChild} from '@angular/core';
import {windowScrollService} from "@core/services/window-scroll.service";
import {EFFECT_TEXT_ON_TOP} from "@core/animation/animation.effects";

@Component({
  selector: 'hope-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss'],
  animations: [...EFFECT_TEXT_ON_TOP]
})
export class InfoComponent extends windowScrollService{
  @ViewChild('infoHostElm') infoHostElm: any = ElementRef;

  @HostListener('window:scroll', []) onWindowScroll() {
    this.isScrolledIntoView(this.infoHostElm);
  }

  constructor() {
    super()
  }
}
