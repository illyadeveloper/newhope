import {Component, OnInit} from '@angular/core';
import SwiperCore, {SwiperOptions, Navigation} from 'swiper';

SwiperCore.use([Navigation]);

const SERVICES_LIST = [
  {
    name: 'IVF',
    icon: 'hpi-ivf'
  },
  {
    name: 'IVF + <br> Egg Donation',
    icon: 'hpi-egg'
  },
  {
    name: 'Surrogacy',
    icon: 'hpi-surrogacy'
  },
  {
    name: 'Surrogacy<br>Egg Donation',
    icon: 'hpi-sur-egg'
  },
  {
    name: 'Frozen embryo<br>Transfer (FET)',
    icon: 'hpi-frozen'
  },
  {
    name: 'Legal Support',
    icon: 'hpi-legal'
  },
  {
    name: 'Pregnancy<br>Support',
    icon: 'hpi-pregnancy-support'
  }
]

@Component({
  selector: 'hope-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent {

  servicesList = SERVICES_LIST;

  config: SwiperOptions = {
    // slidesPerView: 4,
    width: 230,
    spaceBetween: 24,
    loop: true,

    direction: 'horizontal',
    centeredSlides: true,
    slidesPerView: 'auto',
    navigation: true,
    pagination: {clickable: true},
    scrollbar: {draggable: true},

    // navigation: {
    //   nextEl: ".swiper-button-next",
    //   prevEl: ".swiper-button-prev",
    // },
  };

  constructor() {
  }
}
