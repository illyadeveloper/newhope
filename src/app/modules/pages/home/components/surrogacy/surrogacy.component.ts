import {Component, HostListener, ElementRef, ViewChild} from '@angular/core';
import {EFFECT_TEXT_ON_TOP, EFFECT_SCALE, EFFECT_SHOW_BLUR} from "@core/animation/animation.effects";
import {windowScrollService} from "@core/services/window-scroll.service";

@Component({
  selector: 'hope-surrogacy',
  templateUrl: './surrogacy.component.html',
  styleUrls: ['./surrogacy.component.scss'],
  animations: [...EFFECT_TEXT_ON_TOP, ...EFFECT_SCALE, ...EFFECT_SHOW_BLUR]
})
export class SurrogacyComponent extends windowScrollService {
  @ViewChild('surrogacyHostElm') surrogacyHostElm: any = ElementRef;

  @HostListener('window:scroll', []) onWindowScroll() {
    this.isScrolledIntoView(this.surrogacyHostElm);
  }

  constructor() {
    super()
  }
}
