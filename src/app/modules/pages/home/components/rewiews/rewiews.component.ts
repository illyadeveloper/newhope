import {Component, ElementRef, HostListener, ViewChild} from '@angular/core';
import {SwiperOptions} from 'swiper'
import {EFFECT_TEXT_ON_TOP} from "@core/animation/animation.effects";
import {windowScrollService} from "@core/services/window-scroll.service";
import {REVIEWS} from '@core/stores/testimonils.store'

@Component({
  selector: 'hope-rewiews',
  templateUrl: './rewiews.component.html',
  styleUrls: ['./rewiews.component.scss'],
  animations: [EFFECT_TEXT_ON_TOP]
})

export class RewiewsComponent extends windowScrollService {
  @ViewChild('reviewsHostElm') reviewsHostElm: any = ElementRef;

  @HostListener('window:scroll', []) onWindowScroll() {
    this.isScrolledIntoView(this.reviewsHostElm);
  }

  config: SwiperOptions = {
    direction: 'horizontal',
    // slidesPerView: 4,
    speed: 400,
    autoplay: true,
    width: 432,
    spaceBetween: 24,
    loop: true,
    centeredSlides: true,

    slidesPerView: 'auto',
    navigation: true,
    pagination: {clickable: true},
    scrollbar: {draggable: true},

    updateOnWindowResize: true,
    breakpoints: {
      280: {
        slidesPerView: 1,
        spaceBetween: 10,
        width: null,
      },
      640: {
        spaceBetween: 24,
        slidesPerView: 2,

      },
      968: {
        slidesPerView: 'auto',
      },
    }
  };

  lists: any = REVIEWS;

  constructor() {
    super()
  }
}
