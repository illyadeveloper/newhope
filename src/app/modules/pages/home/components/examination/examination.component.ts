import {Component, ElementRef, HostListener, Inject, Injectable, OnInit, PLATFORM_ID, ViewChild} from '@angular/core';
import {windowScrollService} from "@core/services/window-scroll.service";
import {EFFECT_BLOCKS_ON_DOWN} from "@core/animation/animation.effects";
import {isPlatformBrowser} from "@angular/common";
import {NativeRefService} from "@core/refs/native-ref.service";

@Component({
  selector: 'hope-examination',
  templateUrl: './examination.component.html',
  styleUrls: ['./examination.component.scss'],
  animations: EFFECT_BLOCKS_ON_DOWN
})

export class ExaminationComponent extends windowScrollService {

  @ViewChild('examinationEml') examinationEml: any = ElementRef;

  @HostListener('window:scroll', []) onWindowScroll() {
    this.isScrolledIntoParentOffsetView(this.examinationEml);
    // this.isScrolledIntoView(this.examinationEml);
  }

  constructor(
    @Inject(PLATFORM_ID) private platformId: object,
    private native: NativeRefService
  ) {
    super();
  }

  goToLink(url: string) {
    let link = '/assets/files/' + url;

    if (isPlatformBrowser(this.platformId)) {
      this.native.nativeWindow.open(link, "_blank");
    }
  }
}
