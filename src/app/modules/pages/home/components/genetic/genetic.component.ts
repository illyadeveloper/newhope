import {Component, ElementRef, HostListener, ViewChild} from '@angular/core';
import {windowScrollService} from "@core/services/window-scroll.service";
import {EFFECT_TEXT_ON_LEFT_TO_RIGHT, EFFECT_TEXT_ON_TOP} from "@core/animation/animation.effects";

@Component({
  selector: 'hope-genetic',
  templateUrl: './genetic.component.html',
  styleUrls: ['./genetic.component.scss'],
  animations: [...EFFECT_TEXT_ON_LEFT_TO_RIGHT, ...EFFECT_TEXT_ON_TOP]
})
export class GeneticComponent extends windowScrollService {

  @ViewChild('geneticHostElm') geneticHostElm: any = ElementRef;

  @HostListener('window:scroll', []) onWindowScroll() {
    this.isScrolledIntoView(this.geneticHostElm);
  }

  constructor() {
    super()
  }
}
