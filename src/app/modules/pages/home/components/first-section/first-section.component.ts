import {Component} from '@angular/core';
import {EFFECT_SCALE, EFFECT_TEXT_ON_TOP} from "@core/animation/animation.effects";

@Component({
  selector: 'hope-first-section',
  templateUrl: './first-section.component.html',
  styleUrls: ['./first-section.component.scss'],
  animations: [...EFFECT_TEXT_ON_TOP, ...EFFECT_SCALE]
})
export class FirstSectionComponent {

  isAnimation: boolean = false;

  constructor() {
    setTimeout( ()=>{
      this.isAnimation = true;
    },0)
  }
}
