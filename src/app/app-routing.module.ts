import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LayoutRootComponent} from "@modules/layout-root/layout-root.component";

const routes: Routes = [
  {
    path: '',
    component: LayoutRootComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./modules/pages/home/home.module').then(m => m.HomeModule)
      },
      {
        path: 'costs',
        loadChildren: () => import('./modules/pages/prices/prices.module').then(m => m.PricesModule)
      },
      {
        path: 'about-us',
        loadChildren: () => import('./modules/pages/about-us/about-us.module').then(m => m.AboutUsModule)
      },
      {
        path: 'faq',
        loadChildren: () => import('./modules/pages/faq/faq.module').then(m => m.FaqModule)
      },
      {
        path: 'blog',
        loadChildren: () => import('./modules/pages/blog/blog.module').then(m => m.BlogModule)
      },
      {
        path: 'blog/:id',
        loadChildren: () => import('./modules/pages/blog-detail/blog-detail.module').then(m => m.BlogDetailModule)
      },
      {
        path: 'contact',
        loadChildren: () => import('./modules/pages/contact/contact.module').then(m => m.ContactModule)
      },
      {
        path: 'privacy-policy',
        loadChildren: () => import('./modules/pages/privacy/privacy.module').then(m => m.PrivacyModule)
      },
      {
        path: 'surrogacy-programs',
        loadChildren: () => import('./modules/pages/surrogacy-programs/surrogacy-programs.module').then(m => m.SurrogacyProgramsModule)
      },
      {
        path: 'ngs24',
        loadChildren: () => import('./modules/pages/ngs24/ngs24.module').then(m => m.Ngs24Module)
      },
      {
        path: 'pgd',
        loadChildren: () => import('./modules/pages/pgd/pgd.module').then(m => m.PgdModule)
      },
      {
        path: 'nipt',
        loadChildren: () => import('./modules/pages/nipt/nipt.module').then(m => m.NiptModule)
      },
      {
        path: 'hope-404',
        loadChildren: () => import('./modules/pages/hope-404/hope-404.module').then(m => m.Hope404Module)
      },
      {
        path: 'testimonials',
        loadChildren: () => import('./modules/pages/testimonials/testimonials.module').then(m => m.TestimonialsModule)
      }
    ]
  },
  {
    path: 'become-surrogate',
    loadChildren: () => import('./modules/pages/become-surrogate/become-surrogate.module').then(m => m.BecomeSurrogateModule)
  },
  {
    path: 'database',
    loadChildren: () => import('./modules/pages/database/database.module').then(m => m.DatabaseModule)
  },
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full'
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: 'hope-404'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled',
    initialNavigation: 'enabled',
    scrollOffset: [0, 0],
    anchorScrolling: 'enabled',
    relativeLinkResolution: 'legacy'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
