import {Component, HostListener, HostBinding, Inject, OnInit} from '@angular/core';
import {DOCUMENT} from "@angular/common";

@Component({
  selector: 'hope-button-top',
  templateUrl: './button-top.component.html',
  styleUrls: ['./button-top.component.scss']
})
export class ButtonTopComponent {
  isShowButtonTop: boolean = false;

  @HostListener('window:scroll', []) onScroll(): void {
    let wind = this.document.documentElement;

    ((wind.scrollTop > ((wind.scrollHeight - wind.clientHeight) - 350)) && (wind.scrollTop > wind.clientHeight * 1.1))
      ? this.isShowButtonTop = true
      : this.isShowButtonTop = false
  }

  @HostBinding('class.--hide') get hide() {
    return !this.isShowButtonTop;
  }

  constructor(
    @Inject(DOCUMENT) private document: Document
  ) {
  }

  goToTop() {
    document.documentElement.scrollTop = 0;
  }
}
