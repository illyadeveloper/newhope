import {Component, ElementRef, forwardRef, OnInit, ViewChild, Input} from '@angular/core';
import {ControlValueAccessor, NG_VALIDATORS, NG_VALUE_ACCESSOR} from '@angular/forms';
import {ElementBase} from '../../components/element-base';

@Component({
  selector: 'hope-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => InputComponent),
      multi: true
    }
  ]
})
export class InputComponent extends ElementBase implements ControlValueAccessor {

  @Input() name: string = '';
  @Input() type = 'text'
  @Input() autofocus: boolean = false;

  constructor() {
    super();
  }

  public onChange(event: any) {
    this.propagateChange(this.value);
  }

  public onFocus(event: any) {
    this.isFocused = true;
    this.triggerFocus.emit(event);

    // qzsetTimeout(() => {
    //   this.inputRef.nativeElement.selectionStart = 1;
    //   this.inputRef.nativeElement.selectionEnd = 1;
    //   this.inputRef.nativeElement.focus();
    // });
  }

  public onBlur(event: any) {
    this.isFocused = false;
    this.triggerBlur.emit(event);
  }
}
