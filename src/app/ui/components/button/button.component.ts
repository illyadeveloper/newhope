import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'hope-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent {

  @Output() clickButton = new EventEmitter();

  @Input() typeButton = 'submit';
  @Input() disabled = false;

  onClick(event: any): void {
    this.clickButton.emit(event);
  }
}
