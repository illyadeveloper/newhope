import {Component, forwardRef, Input} from '@angular/core';
import {ControlValueAccessor, NG_VALIDATORS, NG_VALUE_ACCESSOR} from "@angular/forms";
import {ElementBase} from "../element-base";

@Component({
  selector: 'hope-textarea',
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TextareaComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => TextareaComponent),
      multi: true
    }
  ]
})
export class TextareaComponent extends ElementBase implements ControlValueAccessor {

  @Input() name: string = '';
  @Input() type: string = 'text';
  @Input() errorMsg: string = '';

  isErrorState: boolean = false;

  constructor() {
    super()
  }

  public onChange(event: any) {
    this.propagateChange(this.value);
    this.error;
  }

  public onFocus(event: any) {
    this.isFocused = true;
    this.triggerFocus.emit(event);
  }

  public onBlur(event: any) {
    this.isFocused = false;
    this.triggerBlur.emit(event);
  }
}
