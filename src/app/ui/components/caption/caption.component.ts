import {Component, Input} from '@angular/core';

@Component({
  selector: 'hope-caption',
  templateUrl: './caption.component.html',
  styleUrls: ['./caption.component.scss']
})
export class CaptionComponent {
  @Input() titleTag: boolean = true;
  @Input() content: string = '';
}
