import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ButtonComponent} from './components/button/button.component';
import {InputComponent} from './components/input/input.component';
import {LogoComponent} from './components/logo/logo.component';
import {ButtonMenuComponent} from './components/button-menu/button-menu.component';
import {CaptionComponent} from './components/caption/caption.component';
import {DescriptionComponent} from './components/description/description.component';
import {TextareaComponent} from './components/textarea/textarea.component';
import {FormsModule} from "@angular/forms";
import { CursorComponent } from './components/cursor/cursor.component';
import { ButtonTopComponent } from './components/button-top/button-top.component';

const COMPONENTS = [
  ButtonComponent,
  InputComponent,
  TextareaComponent,
  LogoComponent,
  ButtonMenuComponent,
  CaptionComponent,
  DescriptionComponent,
  CursorComponent,
  ButtonTopComponent
]

@NgModule({
  declarations: [...COMPONENTS],
  exports: [...COMPONENTS],
  imports: [
    CommonModule,
    FormsModule
  ]
})
export class UiModule {
}
