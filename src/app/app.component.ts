import {Component, Inject, PLATFORM_ID, AfterViewInit} from '@angular/core';
import {LoadingBarService} from "@ngx-loading-bar/core";
import {isPlatformBrowser} from "@angular/common";
import {NativeRefService} from "@core/refs/native-ref.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements AfterViewInit {
  // isTheming: string | null = 'default' || 'normal' || 'newYear';
  isLoadingPage: boolean = false;
  // isChangeCursor: boolean = false;

  isTouch: any = (('ontouchstart' in this.native) || (this.native.nativeWindow.navigator.maxTouchPoints > 0));
  isBrowser = isPlatformBrowser(this.platformId);

  private loader = this.loadingService.useRef();

  constructor(
    @Inject(PLATFORM_ID) private platformId: object,
    private native: NativeRefService,
    private loadingService: LoadingBarService
  ) {
    // this.checkTouchDevices();
    this.loader.start();

    // this.applyNewYearTheme();

    setTimeout(() => {
      this.native.html.setAttribute('loading', 'false');
    }, 100);
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.loader.complete();
      this.isLoadingPage = true;
    }, 500)
  }

  // checkTouchDevices() {
  //   if (this.isBrowser) {
  //     this.isTouch
  //       ? (this.isChangeCursor = false)
  //       : (this.isChangeCursor = true)
  //   }
  // }

  // applyNewYearTheme() {
  //   this.isTheming = 'newYear'
  //   this.native.html.setAttribute('themes', 'new-year');
  // }

  /*  seoAddOg(){
      { name: 'og:title', content: og.title },
      { name: 'og:description', content: og.description },
      { property: 'og:image', content: og.image },
      { property: 'theme-color', name: 'theme-color', content: '#fe4100' },
      { property: 'fb:app_id', content: fbId }
    }*/
}
