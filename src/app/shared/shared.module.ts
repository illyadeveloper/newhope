import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ConsultationComponent} from "./components/consultation/consultation.component";
import {MobileMenuComponent} from "./components/mobile-menu/mobile-menu.component";
import {UiModule} from "@ui/ui.module";
import {FlexLayoutModule} from "@angular/flex-layout";
import {RouterModule} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NgSelectModule} from '@ng-select/ng-select';
import {PolicyCookieComponent} from './components/policy-cookie/policy-cookie.component';
import {RootEffectComponent} from './components/root-effect/root-effect.component';
// import {NgParticlesModule} from "ng-particles";

const COMPONENTS = [
  ConsultationComponent,
  MobileMenuComponent,
  PolicyCookieComponent,
  RootEffectComponent
]

@NgModule({
  declarations: [
    ...COMPONENTS
  ],
  exports: [
    ...COMPONENTS
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    UiModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    // NgParticlesModule
  ]
})

export class SharedModule {
}
