import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'hope-mobile-menu',
  templateUrl: './mobile-menu.component.html',
  styleUrls: ['./mobile-menu.component.scss']
})
export class MobileMenuComponent {

  @Output() closeEvent = new EventEmitter();

  constructor() {
  }

  close(event: any) {
    this.closeEvent.emit(event);
  }
}
