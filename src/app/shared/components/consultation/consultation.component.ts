import {Component, OnInit, Input, Output, HostBinding, EventEmitter} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {contactsStore} from "@core/stores/contacts.store";

import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {ElementBase} from "@ui/components/element-base";
import {Router} from '@angular/router';

const HTTP_OPTIONS = {
  headers: new HttpHeaders({
    // 'Content-Type': 'application/json',
    'Content-Type': 'application/json; charset=utf-8'
  }),
  responseType: 'text' as 'json'
};

// const SUBJECTS = [
//     {id: 1, name: 'IVF'},
//     {id: 2, name: 'Surrogacy Programs'},
//     {id: 3, name: 'Egg Donation'}
// ];
//
const COUNTRIES = [
  "Australia",
  "Canada",
  // "China",
  "Germany",
  // "Greece",
  // "France",
  // "Italy",
  "Ireland",
  // "Sweden",
  "USA",
  "Other country..."
]

// const COUNTRIES = "https://trial.mobiscroll.com/content/countries.json"; //require('~/src/country-by-capital-city.json')
/*const COUNTRIES = [
  "Afghanistan",
  "Albania",
  "Algeria",
  "American Samoa",
  "Andorra",
  "Angola",
  "Anguilla",
  "Antarctica",
  "Antigua and Barbuda",
  "Argentina",
  "Armenia",
  "Aruba",
  "Australia",
  "Austria",
  "Azerbaijan",
  "Bahamas (the)",
  "Bahrain",
  "Bangladesh",
  "Barbados",
  "Belarus",
  "Belgium",
  "Belize",
  "Benin",
  "Bermuda",
  "Bhutan",
  "Bolivia (Plurinational State of)",
  "Bonaire, Sint Eustatius and Saba",
  "Bosnia and Herzegovina",
  "Botswana",
  "Bouvet Island",
  "Brazil",
  "British Indian Ocean Territory (the)",
  "Brunei Darussalam",
  "Bulgaria",
  "Burkina Faso",
  "Burundi",
  "Cabo Verde",
  "Cambodia",
  "Cameroon",
  "Canada",
  "Cayman Islands (the)",
  "Central African Republic (the)",
  "Chad",
  "Chile",
  "China",
  "Christmas Island",
  "Cocos (Keeling) Islands (the)",
  "Colombia",
  "Comoros (the)",
  "Congo (the Democratic Republic of the)",
  "Congo (the)",
  "Cook Islands (the)",
  "Costa Rica",
  "Croatia",
  "Cuba",
  "Curaçao",
  "Cyprus",
  "Czechia",
  "Côte d'Ivoire",
  "Denmark",
  "Djibouti",
  "Dominica",
  "Dominican Republic (the)",
  "Ecuador",
  "Egypt",
  "El Salvador",
  "Equatorial Guinea",
  "Eritrea",
  "Estonia",
  "Eswatini",
  "Ethiopia",
  "Falkland Islands (the) [Malvinas]",
  "Faroe Islands (the)",
  "Fiji",
  "Finland",
  "France",
  "French Guiana",
  "French Polynesia",
  "French Southern Territories (the)",
  "Gabon",
  "Gambia (the)",
  "Georgia",
  "Germany",
  "Ghana",
  "Gibraltar",
  "Greece",
  "Greenland",
  "Grenada",
  "Guadeloupe",
  "Guam",
  "Guatemala",
  "Guernsey",
  "Guinea",
  "Guinea-Bissau",
  "Guyana",
  "Haiti",
  "Heard Island and McDonald Islands",
  "Holy See (the)",
  "Honduras",
  "Hong Kong",
  "Hungary",
  "Iceland",
  "India",
  "Indonesia",
  "Iran (Islamic Republic of)",
  "Iraq",
  "Ireland",
  "Isle of Man",
  "Israel",
  "Italy",
  "Jamaica",
  "Japan",
  "Jersey",
  "Jordan",
  "Kazakhstan",
  "Kenya",
  "Kiribati",
  "Korea (the Democratic People's Republic of)",
  "Korea (the Republic of)",
  "Kuwait",
  "Kyrgyzstan",
  "Lao People's Democratic Republic (the)",
  "Latvia",
  "Lebanon",
  "Lesotho",
  "Liberia",
  "Libya",
  "Liechtenstein",
  "Lithuania",
  "Luxembourg",
  "Macao",
  "Madagascar",
  "Malawi",
  "Malaysia",
  "Maldives",
  "Mali",
  "Malta",
  "Marshall Islands (the)",
  "Martinique",
  "Mauritania",
  "Mauritius",
  "Mayotte",
  "Mexico",
  "Micronesia (Federated States of)",
  "Moldova (the Republic of)",
  "Monaco",
  "Mongolia",
  "Montenegro",
  "Montserrat",
  "Morocco",
  "Mozambique",
  "Myanmar",
  "Namibia",
  "Nauru",
  "Nepal",
  "Netherlands (the)",
  "New Caledonia",
  "New Zealand",
  "Nicaragua",
  "Niger (the)",
  "Nigeria",
  "Niue",
  "Norfolk Island",
  "Northern Mariana Islands (the)",
  "Norway",
  "Oman",
  "Pakistan",
  "Palau",
  "Palestine, State of",
  "Panama",
  "Papua New Guinea",
  "Paraguay",
  "Peru",
  "Philippines (the)",
  "Pitcairn",
  "Poland",
  "Portugal",
  "Puerto Rico",
  "Qatar",
  "Republic of North Macedonia",
  "Romania",
  "Russian Federation (the)",
  "Rwanda",
  "Réunion",
  "Saint Barthélemy",
  "Saint Helena, Ascension and Tristan da Cunha",
  "Saint Kitts and Nevis",
  "Saint Lucia",
  "Saint Martin (French part)",
  "Saint Pierre and Miquelon",
  "Saint Vincent and the Grenadines",
  "Samoa",
  "San Marino",
  "Sao Tome and Principe",
  "Saudi Arabia",
  "Senegal",
  "Serbia",
  "Seychelles",
  "Sierra Leone",
  "Singapore",
  "Sint Maarten (Dutch part)",
  "Slovakia",
  "Slovenia",
  "Solomon Islands",
  "Somalia",
  "South Africa",
  "South Georgia and the South Sandwich Islands",
  "South Sudan",
  "Spain",
  "Sri Lanka",
  "Sudan (the)",
  "Suriname",
  "Svalbard and Jan Mayen",
  "Sweden",
  "Switzerland",
  "Syrian Arab Republic",
  "Taiwan",
  "Tajikistan",
  "Tanzania, United Republic of",
  "Thailand",
  "Timor-Leste",
  "Togo",
  "Tokelau",
  "Tonga",
  "Trinidad and Tobago",
  "Tunisia",
  "Turkey",
  "Turkmenistan",
  "Turks and Caicos Islands (the)",
  "Tuvalu",
  "Uganda",
  "Ukraine",
  "United Arab Emirates (the)",
  "United Kingdom of Great Britain and Northern Ireland (the)",
  "United States Minor Outlying Islands (the)",
  "United States of America (the)",
  "Uruguay",
  "Uzbekistan",
  "Vanuatu",
  "Venezuela (Bolivarian Republic of)",
  "Viet Nam",
  "Virgin Islands (British)",
  "Virgin Islands (U.S.)",
  "Wallis and Futuna",
  "Western Sahara",
  "Yemen",
  "Zambia",
  "Zimbabwe",
  "Åland Islands"
];*/
const MARITAL_STATUS = [
  'Married',
  // 'Widowed',
  // 'Separated',
  // 'Divorced',
  'Single',
];
const URL_SENDING_ACTIVE = '/assets/sending/order.php';

@Component({
  selector: 'hope-consultation',
  templateUrl: './consultation.component.html',
  styleUrls: ['./consultation.component.scss']
})
export class ConsultationComponent extends ElementBase implements OnInit {

  @Input() toggleMenu: (boolean | null) = false;
  @Output() hideEvent = new EventEmitter();

  openHash: string = '?consultation:true=';

  @HostBinding('class.--open') get openMenu(): (boolean | null) {
    return this.toggleMenu;
  }

  @HostBinding('class.--open') get hasRoute(): any {
    return ((this.router.url.includes(this.openHash)) || this.toggleMenu);
  }

  selectMaritalStatusOptions: any = MARITAL_STATUS;
  selectCountryOptions: any = COUNTRIES;

  isSubmitted: boolean = false;
  form: any = FormGroup;

  isSendingStatus: boolean = false;
  diffCountry: boolean = false;
  isSingle: boolean = false;

  hostEmail: any = {hostMail: contactsStore.EMAIL};

  constructor(
    private fb: FormBuilder,
    private httpClient: HttpClient,
    private router: Router,
  ) {
    super();
  }

  private createForm(): void {
    this.form = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.required, Validators.email]],
      // phones: ['', [Validators.required, Validators.pattern('[- +()0-9]+'), Validators.minLength(5)]],
      status: ['', [Validators.required, Validators.minLength(3)]],
      country: ['', [Validators.required, Validators.minLength(3)]],
      countryOther: ['', [Validators.required, Validators.minLength(3)]],
      age: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(3), Validators.min(18), Validators.max(110)]],
      // subjects: ['', [Validators.required, Validators.minLength(3)]],
      message: ['', [Validators.required, Validators.minLength(12)]]
    });

    this.form.reset();
  }

  ngOnInit(): void {
    this.createForm();
  }

  public sendDataPost(url: string, data: any) {
    let body = JSON.stringify(data);

    return this.httpClient.post(url, body, HTTP_OPTIONS)
      .subscribe(
        (result) => {
          console.log('Successfully sending');
          this.isSendingStatus = true;
        },

        (error) => {
          console.log('Error sending');
          console.log('result', error);
        }
      );
  }

  closeConsultation(event: any) {
    this.toggleMenu = false;
    this.hideEvent.emit(event);

    if (this.router.url.includes(this.openHash)) {
      this.router.navigate([this.router.url.replace(this.openHash, '')]);
      return
    }
  }

  closeMessage() {
    if (!this.isSendingStatus) {
      return
    }

    return setTimeout(() => {
      this.isSendingStatus = false;
      this.form.reset();
    }, 900);
  }

  selectMaritalStatus(evn?: any) {
    evn == "Single"
      ? this.isSingle = true
      : this.isSingle = false
  }

  selectCountry(evn?: any) {
    if (evn == "Other country...") {
      this.diffCountry = true
      this.form.controls['countryOther'].reset()
      this.form.controls['countryOther'].setErrors({'incorrect': true})
    } else {
      this.diffCountry = false;
      this.form.controls['countryOther'].reset()
      this.form.controls['countryOther'].setErrors(null)
    }
    return
  }

  getCountry: any = (): string => {
    return (this.form.value.countryOther == null || this.form.value.countryOther === '')
      ? this.form.value.country
      : this.form.value.countryOther
  };

  onSubmit(event?: any): void {
    let data: any = {};

    if (this.form.invalid) {
      this.isSubmitted = true;
      return;
    }

    data = {
      name: this.form.value.name,
      email: this.form.value.email,
      age: this.form.value.age,
      status: this.form.value.status,
      country: this.getCountry(),
      message: this.form.value.message,
      ...this.hostEmail
    };

    setTimeout(() => {
      this.sendDataPost(URL_SENDING_ACTIVE, data);
    }, 300)

    this.isSubmitted = false;
  }
}
