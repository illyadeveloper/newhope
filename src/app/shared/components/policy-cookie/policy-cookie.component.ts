import {Component, OnInit} from '@angular/core';

import {CookieConsentService} from "@core/services/cookie-consent.service";

@Component({
  selector: 'hope-policy-cookie',
  templateUrl: './policy-cookie.component.html',
  styleUrls: ['./policy-cookie.component.scss'],
})
export class PolicyCookieComponent implements OnInit {

  constructor(private cookieConsent: CookieConsentService
  ) {
  }

  ngOnInit(): void {
  }

  accept($event?: any) {
    $event.preventDefault();

    this.cookieConsent.acceptCookies();
  }

  decline($event?: any) {
    $event.preventDefault();

    this.cookieConsent.declineCookies();
  }
}
