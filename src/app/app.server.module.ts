import {NgModule} from '@angular/core';
import {ServerModule, ServerTransferStateModule} from '@angular/platform-server';

import {AppModule} from './app.module';
import {AppComponent} from './app.component';
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
// import {TransferState} from "@angular/platform-browser";
import {FlexLayoutServerModule} from "@angular/flex-layout/server";
// import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
// import { translateServerLoaderFactory } from '@app/translate-server.loader';
import {NgxSsrTimeoutModule} from '@ngx-ssr/timeout';

@NgModule({
  imports: [
    AppModule,
    ServerModule,
    NoopAnimationsModule,
    ServerTransferStateModule,
    NgxSsrTimeoutModule.forRoot({timeout: 500}),
    FlexLayoutServerModule,
    // TranslateModule.forRoot({
    //   loader: {
    //     provide: TranslateLoader,
    //     useFactory: translateServerLoaderFactory,
    //     deps: [TransferState]
    //   }
    // })
  ],
  bootstrap: [AppComponent]
})
export class AppServerModule {
}
