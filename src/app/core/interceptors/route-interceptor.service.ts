import {Injectable} from '@angular/core';
import {
  Router,
  Event as RouterEvent,
  NavigationStart,
  NavigationEnd,
  NavigationCancel,
  NavigationError
} from '@angular/router';
import {LoadingBarService} from "@ngx-loading-bar/core";
import {Observable, Subject} from "rxjs";

@Injectable()
export class RouteInterceptorService {
  private isPendingRouteSubject = new Subject<boolean>();
  private loader = this.loadingBar.useRef();

  constructor(
    private router: Router,
    private loadingBar: LoadingBarService,
  ) {
    router.events.subscribe((event: any) => {
      this.interceptRoute(event);
      // console.log('RouterEvent End');
    })
  }

  get isRouteStage(): Observable<boolean> {
    return this.isPendingRouteSubject.asObservable();
  }

  private loaderStart() {
    this.loader.increment(0);
    this.loader.start();
  }

  private loaderFinish() {
    this.loadingBar.useRef().increment(0);
    this.loadingBar.useRef().complete()
  }

  private interceptRoute(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      this.loaderStart();
    }

    if (event instanceof NavigationEnd || event instanceof NavigationCancel || event instanceof NavigationError) {
      this.loaderFinish();
    }
  }
}
