import {animate, keyframes, style, state, transition, trigger} from "@angular/animations";

const DURATION = 2.33;
const DELAY = 0.05;
const TIMING = "cubic-bezier(0,.62,.36,1.49)";

export const EFFECT_TEXT_ON_TOP = [
  trigger('showTextOnTop', [
    state('false', style({opacity: 0, transform: 'translateY(10vh) scaleY(0.8)'})),
    state('true', style({opacity: 1, transform: 'translateY(0) scaleY(1)'})),

    transition('* => *', [
      animate(`${DURATION}s ${DELAY}s ${TIMING}`)
    ])
  ]),
  trigger('showTextOnTopSecond', [
    state('false', style({opacity: 0, transform: 'translateY(10vh) scaleY(0.8)'})),
    state('true', style({opacity: 1, transform: 'translateY(0) scaleY(1)'})),

    transition('* => *', [
      animate(`${DURATION * 1.3}s ${DELAY * 1.2}s ${TIMING}`)
    ])
  ]),
  trigger('showTextOnTopThird', [
    state('false', style({opacity: 0, transform: 'translateY(10vh) scaleY(0.8)'})),
    state('true', style({opacity: 1, transform: 'translateY(0) scaleY(1)'})),

    transition('* => *', [
      animate(`${DURATION * 1.5}s ${DELAY * 1.5}s ${TIMING}`)
    ])
  ])
]

export const EFFECT_TEXT_ON_LEFT_TO_RIGHT = [
  trigger('showTextOnLeftToRight', [
    state('false', style({opacity: 0, transform: 'translateX(-40vw) scaleX(0.8)'})),
    state('true', style({opacity: 1, transform: 'translateX(0) scaleX(1)'})),

    transition('* => *', [
      animate(`${DURATION}s ${DELAY}s ${TIMING}`)
    ])
  ])
]

export const EFFECT_SCALE = [
  trigger('scaleElem', [
    state('false', style({opacity: 0, transform: 'scale(0)'})),
    state('true', style({opacity: 1, transform: 'scale(1)'})),

    transition('* => *', [
      animate(`${DURATION}s ${DELAY * 10}s ${TIMING}`)
    ])
  ])
]

export const EFFECT_SHOW_BLUR = [
  trigger('showFade', [
    state('false', style({opacity: 0, transform: 'scale(0)'})),
    state('true', style({opacity: 1, transform: 'scale(1)'})),

    transition('* => *', [
      animate(`${DURATION - 1.2}s ${0}s linear`)
    ])
  ])
]


export const EFFECT_BLOCK_ON_DOWN = [
  trigger('showBlockOnDown', [
    state('false', style({opacity: 0, transform: 'translateY(-20vh)'})),
    state('true', style({opacity: 1, transform: 'translateY(0)'})),

    transition('* => *', [
      animate(`${DURATION}s ${DELAY}s linear`)
    ])
  ])
]


export const EFFECT_BLOCK_ON_TOP = [
  trigger('showBlockOnTop', [
    state('false', style({opacity: 0, transform: 'translateY(20vh) scale(0.8)'})),
    state('true', style({opacity: 1, transform: 'translateY(0) scale(1)'})),

    transition('* => *', [
      animate(`${DURATION}s ${DELAY}s ${TIMING}`)
    ])
  ])
]

export const EFFECT_SLIDE_ON_RIGhT = [
  trigger('slideOnRight', [
    state('false', style({opacity: 0, transform: 'translateX(100vw) scale(0)'})),
    state('true', style({opacity: 1, transform: 'translateX(0)  scale(1)'})),

    transition('* => *', [
      animate('0.8s 1.2s  cubic-bezier(0,.3,.55,.61)')
    ])
  ]),
  trigger('slideOnRightSecond', [
    state('false', style({opacity: 0, transform: 'translateX(110vw) scale(0)'})),
    state('true', style({opacity: 1, transform: 'translateX(0)  scale(1)'})),

    transition('* => *', [
      animate('0.95s 1.4s cubic-bezier(0,.3,.55,.61)')
    ])
  ]),
  trigger('slideOnRightThird', [
    state('false', style({opacity: 0, transform: 'translateX(130vw) scale(0)'})),
    state('true', style({opacity: 1, transform: 'translateX(0)  scale(1)'})),

    transition('* => *', [
      animate('1.05s 1.6s cubic-bezier(0,.3,.55,.61)')
    ])
  ]),
  trigger('slideOnRightFour', [
    state('false', style({opacity: 0, transform: 'translateX(140vw) scale(0)'})),
    state('true', style({opacity: 1, transform: 'translateX(0)  scale(1)'})),

    transition('* => *', [
      animate('1.15s 1.8s cubic-bezier(0,.3,.55,.61)')
    ])
  ])
]

export const EFFECT_BLOCKS_ON_DOWN = [
  trigger('showBlocksOnDown', [
    state('false', style({opacity: 0, transform: 'translateY(10vh) scale(0.8)'})),
    state('true', style({opacity: 1, transform: 'translateY(0) scale(1)'})),
    transition('* => *', [
      // animate('1s 1.2s cubic-bezier(.55,.51,.31,.63)')
      animate(`${DURATION - 0.1}s ${DELAY * 1.1}s  ${TIMING}`)
    ])
  ]),
  trigger('showBlocksOnDownSecond', [
    state('false', style({opacity: 0, transform: 'translateX(10vw) scale(0.8)'})),
    state('true', style({opacity: 1, transform: 'translateX(0) scale(1)'})),
    transition('* => *', [
      // animate('1s 1.4s cubic-bezier(.55,.51,.31,.63)')
      animate(`${DURATION - 0.1}s ${DELAY * 1.2}s ${TIMING}`)
    ])
  ]),
  trigger('showBlocksOnDownThird', [
    state('false', style({opacity: 0, transform: 'translateY(-10vh) scale(0.8)'})),
    state('true', style({opacity: 1, transform: 'translateY(0) scale(1)'})),
    transition('* => *', [
      // animate('1s 1.6s cubic-bezier(.55,.51,.31,.63)')
      animate(`${DURATION - 0.1}s ${DELAY * 1.3}s ${TIMING}`)
    ])
  ])
]

export const EFFECT_SHOW_UP = [
  trigger('showHide', [
    // state('false', style({transform: 'translate(-50%, 50vh)'})),
    // state('true', style({transform: 'translate(-50%, 0vh)'})),

    transition(':enter', [
      style({transform: 'translate(-50%, 50vh)'}),
      animate(`${DURATION}s ${3}s linear`, style({transform: 'translate(-50%, 0vh)'}))
    ]),

    transition(':leave', [
      style({transform: 'translate(-50%, 0vh)'}),
      animate(`${DURATION}s ${DELAY}s linear`, style({transform: 'translate(-50%, 50vh)'}),)
    ])
  ])
  // trigger('myInsertRemoveTrigger', [
  //   transition('void =>', [
  //     style({ opacity: 0, transform: 'translate(-50%, 50vh)' }),
  //     animate(`${DURATION}s ${DELAY * 10}s linear`, style({ opacity: 1, transform: 'translate(-50%, 0vh)' })),
  //   ]),
  //   transition(':leave', [
  //     animate(`${DURATION}s ${DELAY * 10}s linear`, style({ opacity: 0, transform: 'translate(-50%, 50vh)' }))
  //   ])
  // ]),
]

// //  animation: 2s slide-is 2s forwards alternate linear;
// export const EFFECT_HIDE_DOWN = [
//   trigger('hideDown', [
//     state('false', style({opacity: 1, transform: 'translate(-50%, 0vh)'})),
//     state('true', style({opacity: 0, transform: 'translate(-50%, 50vh'})),
//
//     transition('* => *', [
//       animate(`${DURATION}s ${DELAY * 10}s linear`)
//     ])
//   ])
// ]

