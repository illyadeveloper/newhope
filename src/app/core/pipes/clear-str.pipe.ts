import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'clearStr'
})
export class ClearStrPipe implements PipeTransform {

  // transform(value: string, ...args: string[]): string {
  //   return null;
  // }

  transform(str: any): any {
    return str.split(' ').join('_').split(':').join('').split('(').join('').split(')').join('').toLowerCase()
  }

}
