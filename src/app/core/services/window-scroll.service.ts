import {ElementRef, Injectable} from '@angular/core';

@Injectable()
export class windowScrollService {
  isStartAnimate: boolean = false;

  private doc: any = document.documentElement;

  public isScrolledIntoView(elem: ElementRef<HTMLElement>, inc: number = 0): boolean {
    let scrollPos = this.doc.scrollTop;
    let posElm = elem.nativeElement.offsetTop;
    let win = this.doc.clientHeight;

    if ((scrollPos + win + inc) >= posElm) {
      return this.isStartAnimate = true;
    }

    return false;
  }

  public isScrolledIntoParentOffsetView(elem: ElementRef<HTMLElement>, inc: number = 0): boolean {
    let scrollPos = this.doc.scrollTop;
    let posElm = this.getParentOffset(elem);
    let win = this.doc.clientHeight;

    if ((scrollPos + win + inc) >= posElm) {

      return this.isStartAnimate = true;
    }
    return false;
  }

  public getParentOffset(el: ElementRef): number {
    if (el.nativeElement.offsetParent) {
      return el.nativeElement.offsetParent.offsetTop;
    } else {
      return 0;
    }
  }
}
