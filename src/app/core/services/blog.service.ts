import {Inject, Injectable, Optional, PLATFORM_ID} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, BehaviorSubject} from "rxjs";
// lastValueFrom
import {isPlatformBrowser} from "@angular/common";

@Injectable({
  providedIn: 'root'
})
export class BlogService {
  // blogData$: BehaviorSubject<any> = new BehaviorSubject<boolean>([]);
  private serverUrl: string = 'http://localhost:4200';

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    // @Optional() @Inject('serverUrl') protected serverUrl: string,
    private httpClient: HttpClient) {
  }

  private getData(): Observable<any[]> {
    let baseUrl: any = isPlatformBrowser(this.platformId) ? '' : this.serverUrl;
    // console.log('baseUrl= ',baseUrl );

    // return this.httpClient.get<any>( '/assets/static/file.json');
    // console.log('= ', this.httpClient.get<any>(baseUrl + "/assets/json/blog.json"));

    return this.httpClient.get<any>(baseUrl + "/assets/json/blog.json")
  }

  public async getBlogData() {
    let data$ = this.getData();

    return await data$.toPromise().then(res => {
      // console.log('r = ', r);
      // console.log('serverUrl blog.json===', this.httpClient.get<any>(this.serverUrl + "assets/json/blog.json"));
      //
      // console.log('serverUrl ============ ', this.serverUrl);
      return res
    })
    // return await lastValueFrom(data$);
  }
}
