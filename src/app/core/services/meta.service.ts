import {Injectable} from '@angular/core';
import {NativeRefService} from "@core/refs/native-ref.service";
import {environment} from "@environments/environment";
import {Meta, Title} from "@angular/platform-browser";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class MetaService {

  // private pageUrl: string = this.native.nativeDocument.URL;
  private pageUrl: string = 'https://' + environment.projectSiteLink + this.route.url;

  constructor(
    private native: NativeRefService,
    protected meta: Meta,
    protected caption: Title,
    private  route: Router
  ) {

    this.route.events.subscribe((event) => {
      this.pageUrl = 'https://' + environment.projectSiteLink + this.route.url;
    })
  }

  public createCanonicalURL(): void {
    let link: HTMLLinkElement = this.native.nativeDocument.createElement('link');

    this.native.nativeDocument.head.appendChild(link);

    link.setAttribute('rel', 'canonical');
    link.setAttribute('href', this.pageUrl);
    // link.setAttribute('href', this.native.nativeDocument.URL);
  }

  // removeCanonicalURL() {
  //   const canonical: any = this.native.nativeDocument.querySelectorAll('link [rel=”canonical”]');
  //   canonical[0].parentElement.removeChild(canonical[0]);
  // }

  public updateFeedsURL(fileName: string): void {
    let atom: any = this.native.nativeDocument.querySelector("link[type='application/atom+xml']");
    let rss: any = this.native.nativeDocument.querySelector("link[type='application/rss+xml']");

    atom.setAttribute('href', 'assets/feeds/' + fileName + '.xml');
    rss.setAttribute('href', 'assets/feeds/' + fileName + '.rss');
    // rss.setAttribute('rel', 'canonical');
  }

  // public createMeta(data?: any, ogParam?: boolean): void {
  //   this.caption.setTitle(data.TITLE);
  //
  //   this.meta.addTag({name: 'title', content: data.TITLE});
  //   this.meta.addTag({name: 'description', content: data.DESCRIPTION});
  //   this.meta.addTag({name: 'keywords', content: data.KEYWORDS});
  //
  //   if (ogParam) {
  //     this.meta.addTag({property: 'og:site_name', content: environment.projectName});
  //
  //     this.meta.addTag({property: 'og:title', content: data.TITLE});
  //     this.meta.addTag({property: 'og:description', content: data.DESCRIPTION});
  //     this.meta.addTag({property: 'og:url', content: this.pageUrl});
  //     this.meta.addTag({
  //       property: 'og:image',
  //       content: 'https://' + environment.projectSiteLink + '/assets/img/' + data.IMG
  //     });
  //
  //     this.meta.addTag({property: 'og:image:width', content: '1000'});
  //     this.meta.addTag({property: 'og:image:height', content: '667'});
  //   }
  // }

  public updateMeta(data?: any, ogParam?: boolean): void {
    this.caption.setTitle(data.TITLE);

    this.meta.updateTag({name: 'title', content: data.TITLE});
    this.meta.updateTag({name: 'description', content: data.DESCRIPTION});
    this.meta.updateTag({name: 'keywords', content: data.KEYWORDS});

    if (ogParam) {
      //og:image:secure_url
      // <meta property="og:type" content="article"> for blog
      this.meta.updateTag({property: 'og:site_name', content: environment.projectName});
      this.meta.updateTag({property: 'og:title', content: data.TITLE});
      this.meta.updateTag({property: 'og:description', content: data.DESCRIPTION});
      this.meta.updateTag({name: 'twitter:site_name', content: environment.projectName});
      this.meta.updateTag({name: 'twitter:title', content: data.TITLE});
      this.meta.updateTag({name: 'twitter:description', content: data.DESCRIPTION});

      this.meta.updateTag({
        property: 'og:image', content: 'https://' + environment.projectSiteLink + '/assets/img/' + data.IMG
      });

      this.meta.updateTag({
        name: 'twitter:image', content: 'https://' + environment.projectSiteLink + '/assets/img/' + data.IMG
      });

      setTimeout(() => {
        this.meta.updateTag({property: 'og:url', content: this.pageUrl});
        this.meta.updateTag({name: 'twitter:url', content: this.pageUrl});
      }, 0);
    }
  }

  // public removeMeta() {
  //   this.meta.removeTag('title');
  //   this.meta.removeTag('description');
  //   this.meta.removeTag('keywords');
  //
  //   // this.meta.removeTag('og:site_name');
  //   this.meta.removeTag('og:title');
  //   this.meta.removeTag('og:description');
  //
  //   this.meta.removeTag('og:url');
  //   this.meta.removeTag('og:image');
  // }
}
