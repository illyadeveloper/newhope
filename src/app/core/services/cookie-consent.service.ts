import {Inject, Injectable, PLATFORM_ID} from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {isPlatformBrowser} from "@angular/common";
import {NativeRefService} from "@core/refs/native-ref.service";

const CONSENT_NAME = "consent_status";

@Injectable({
  providedIn: 'root'
})

export class CookieConsentService {
  triggerCookieWindow$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  isBrowser = isPlatformBrowser(this.platformId);

  constructor(
    @Inject(PLATFORM_ID) private platformId: object,
    private native: NativeRefService
  ) {
    if (this.isBrowser) {
      this.checkStatus();
    }
  }

  private sessionStorage: any = this.native.nativeWindow.sessionStorage;
  private localStorage: any = this.native.nativeWindow.localStorage;
  private cookieStorage: any = {
    getItem: (key?: any) => {
      const cookie = this.native.nativeDocument.cookie
        .split(';')
        .map(cookie => cookie.split('='))
        .reduce((acc, [key, value]) => ({...acc, [key.trim()]: value}))
      return cookie[key]
    },
    setItem: (key?: any, value?: any) => {
      this.native.nativeDocument.cookie = `${key} = ${value}`
    }
  }

  private checkStatus() {
    (!this.sessionStorage.getItem(CONSENT_NAME)
      && (!this.cookieStorage.getItem(CONSENT_NAME)
        && !this.localStorage.getItem(CONSENT_NAME)))
      ? this.consentOpen()
      : this.consentClose();
  }

  public consentOpen(): void {
    this.triggerCookieWindow$.next(true);
  }

  public consentClose(): void {
    this.triggerCookieWindow$.next(false);
  }

  public acceptCookies(): void {
    this.localStorage.setItem(CONSENT_NAME, true);
    this.cookieStorage.setItem(CONSENT_NAME, true);

    this.consentClose();
  }

  public declineCookies(): void {
    this.sessionStorage.setItem(CONSENT_NAME, true);
    this.consentClose();
  }
}
