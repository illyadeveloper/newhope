import {Inject, Injectable, PLATFORM_ID} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {isPlatformBrowser} from "@angular/common";
import {NativeRefService} from "@core/refs/native-ref.service";

@Injectable({
  providedIn: 'root'
})

export class AsideService {
  isConsultationShow$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  scrollYKeep: number = 0;
  isBrowser = isPlatformBrowser(this.platformId);

  constructor(
    @Inject(PLATFORM_ID) private platformId: object,
    private native: NativeRefService
  ) {
  }

  public scrollHide() {
    this.scrollYKeep = this.native.nativeWindow.scrollY;

    setTimeout(() => {
      this.native.html.classList.add('gm-scroll-hide');
      this.native.body.classList.add('gm-scroll-hide', 'gm-scroll-lock');
    }, 100);

    // Get the current page scroll position  this.document.pageYOffset || this.document.pageXOffset ||
    // let scrollTop = this.document.documentElement.scrollTop,
    //   scrollLeft = this.document.documentElement.scrollLeft;

    // this.html.classList.add('gm-scroll-unset');

    // window.onscroll = () => window.scrollTo(scrollLeft, scrollTop);
  }

  public scrollReset() {
    this.native.html.classList.remove('gm-scroll-hide');
    this.native.body.classList.remove('gm-scroll-hide', 'gm-scroll-lock');

    if (this.isBrowser) {
      this.native.nativeWindow.scrollTo(0, this.scrollYKeep);
    }


    // this.html.classList.add('gm-scroll-unset');
    // window.onscroll = () => {
    // }
  }

  // public syncHeight() {
  //   document.documentElement.style.setProperty(
  //     "--window-inner-height",
  //     `${window.innerHeight}px`
  //   );
  //
  //   // window.addEventListener("resize", syncHeight);
  // }

  public consultationAsideOpen() {
    this.isConsultationShow$.next(true);
    this.scrollHide();

    return true;
  }

  public consultationAsideHide() {
    this.isConsultationShow$.next(false);
    this.scrollReset();

    return false;
  }
}
