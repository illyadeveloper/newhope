import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { ClearStrPipe } from './pipes/clear-str.pipe';

@NgModule({
  declarations: [
    ClearStrPipe
  ],
  imports: [
    CommonModule
  ],
  exports: []
})
export class CoreModule {
}
