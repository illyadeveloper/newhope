export const REVIEWS = [
  {
    name: 'Heidi',
    text: 'All the feedback on the groups on her and New Hope was positive, which meant a lot to us. ',
    data: '16.08.15'
  },
  {
    name: 'Emma',
    text: 'Dear Julia, you have made our year! We are overjoyed to see the report, sonogram and video. We love the video, such a nice touch. Emma',
    data: '12.05.21'
  },
  {
    name: 'Chris',
    text: 'The girls bring us so much joy, laughter and love every day. We are very fortunate that we had you there to support our incredible surrogacy journey - memories we will cherish forever.',
    data: '6.10.19',
  },
  {
    name: 'Charlotte',
    text: 'Dear friend Julia, every day I pinch myself because of my perfect little humans. But today in particular, their birthday, I am even more grateful 2 years of blessings.',
    data: '23.07.19'
  },
  {
    name: 'Ian',
    text: 'They are beautiful (of course we are a little biased). But they wouldn t be here at all without you, and for that we will be forever grateful.',
    data: '12.10.19'
  }
]
