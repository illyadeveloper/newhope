export const PROGRAMS_LIST = [
  {
    name: 'IVF',
    price: '$7 500 - $8 500',
    icon: 'ivf',
    description: 'Average cost of IVF ranges from <b>$7,500</b> to <b>$8,500</b>. This includes the cost of IVF and fertility treatments, the IVF agency fee as well as pricing for legal and other expenses.',
    details: null
  },

  {
    name: 'IVF + Egg Donation',
    price: '$9 500 - $10 000',
    icon: 'egg',
    description: 'Average cost of IVF + Egg Donation ranges from <b>$9,500</b> to <b>$10,000</b> which is based on varying services and fees that may be required for each individual situation. This includes the cost of fertility treatment, Egg donor matching, Egg Donor compensation, the agency fee as well as pricing for legal, travel and other expenses.',
    details: null
  },

  {
    name: 'Surrogacy',
    price: 'from $43 000 ',
    icon: 'surrogacy',
    description:
      'Intended Parents should budget for an average cost of surrogacy ranging from <b>$43,000</b>, with an understanding the range is based on varying services and fees that may be required for each individual situation. This includes the surrogate mother compensation,  the surrogacy agency fee, the cost of IVF and fertility treatments as well as pricing for legal, medication and other expenses.\n' +
      '\n' +
      'We recognize this is an expensive process, but we are committed to protecting your financial and emotional investments, and are pleased to offer a platform of transparency and security for your journey to parenthood. A comprehensive price schedule, detailing the costs of using a surrogate mother and egg donor, will be provided during your individual consultation.',
    details: null
  },

  {
    name: 'Surrogacy + Egg Donation',
    price: 'from $48 000',
    icon: 'sur-egg',
    description: 'The average cost of surrogacy with donor eggs can range from <b>$48,000</b> depending on the individual arrangements. This includes the surrogate mother compensation, egg donor package, the surrogacy agency fee, the cost of IVF and fertility treatments as well as pricing for legal, medication and other expenses. \n' +
      '\n' +
      'Agency fees cover all of the services provided by the agency, including screenings, support and education, legal services, counseling, advertising, marketing and more. New Hope fees remain fixed, regardless of how long it takes to complete the surrogacy process.',
    details: null
  }
]
