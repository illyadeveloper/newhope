export enum contactsStore {
  PHONE = '+380 (68) 802 80 80',
  PHONE_LINK = '380688028080', //380445455824
  // PHONE_SECOND = '+380 (44) 545 58 24',
  // PHONE_SECOND_LINK = '380445455824',
  EMAIL = 'info@newsurrogacy.com',

  ADDRESS = '19A Dniprovska Embankment, office 203, Kiev, Ukraine',
  ADDRESS_INDEX = '02081',
  ADDRESS_LINK = 'https://www.google.com/maps/place/%D0%94%D0%BD%D1%96%D0%BF%D1%80%D0%BE%D0%B2%D1%81%D1%8C%D0%BA%D0%B0+%D0%BD%D0%B0%D0%B1%D0%B5%D1%80%D0%B5%D0%B6%D0%BD%D0%B0,+19,+203,+%D0%9A%D0%B8%D1%97%D0%B2,+02000/@50.4076231,30.6090532,17z/data=!3m1!4b1!4m5!3m4!1s0x40d4c5a308ce3895:0x900ff5f2c33c41a1!8m2!3d50.4076231!4d30.6112419',
  POSITION_LAT = 50.407732482822055,
  POSITION_LNG = 30.61129554245377,

  SOC_FB = 'https://www.facebook.com/ukraine.surrogacy',
  SOC_LN = 'https://www.linkedin.com/company/new-hope-llc',
  SOC_INST = 'https://www.instagram.com/newhopeukraine',
  SOC_TG = 'https://t.me/newhopeukraine',
  SOC_VB = 'viber://chat?number=80688028080',
  SOC_WA = 'https://wa.me/380688028080',
}
