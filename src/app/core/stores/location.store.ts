import {contactsStore} from "@core/stores/contacts.store";
import {environment} from "@environments/environment";

export const MAP_OPTION: any = {
  // mapTypeId: 'hybrid',
  center: {lat: contactsStore.POSITION_LAT, lng: contactsStore.POSITION_LNG},
  scrollwheel: false,
  disableDoubleClickZoom: true,
  // mozCancelFullScreen: true,
  // msExitFullscreen: true,
  disableDefaultUI: true,
  zoomControl: true,
  scaleControl: false,
  maxZoom: 15,
  minZoom: 8,
  styles: [
    {
      "featureType": "administrative",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#444444"
        }
      ]
    },
    {
      "featureType": "landscape",
      "elementType": "all",
      "stylers": [
        {
          "color": "#ffffff"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "all",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "all",
      "stylers": [
        {
          "saturation": -100
        },
        {
          "lightness": 45
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "all",
      "stylers": [
        {
          "visibility": "simplified"
        }
      ]
    },
    {
      "featureType": "road.arterial",
      "elementType": "labels.icon",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "transit",
      "elementType": "all",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "all",
      "stylers": [
        {
          "color": "#ff99b6"
        },
        {
          "visibility": "on"
        }
      ]
    }
  ]
};

export const MAP_MARKER_OPTIONS: any = {
  position: {lat: contactsStore.POSITION_LAT, lng: contactsStore.POSITION_LNG},
  title: environment.projectName,
  icon: 'assets/img/marker.svg',
};

