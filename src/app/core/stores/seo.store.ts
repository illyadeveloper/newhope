// export const IMG_DEFAULT: any = 'brand.jpg';

export enum seoHomePage {
  TITLE = 'New Hope - international surrogacy agency in Ukraine | Surrogacy Ukraine',
  KEYWORDS = "gestational surrogacy, ivf + and surrogacy services, surrogacyukraine, new hope, surrogacy, surrogacy in ukraine, surrogacy agency, clinic, ivf, nipt testing, kiev clinic, average cost of surrogacy, cost of surrogacy, egg donation, surrogacy meaning, what is surrogacy, fertility, all-inclusive, 2 benefits of surrogacy",
  DESCRIPTION = 'Our agency provides IVF and surrogacy services couples from all over the world. We provide the best surrogacy services in Ukraine',
  IMG = 'm_main.jpg'
}

export enum seoPricesPage {
  TITLE = 'Costs surrogacy, donor eggs, ivf in ukraine - New Hope',
  KEYWORDS = "surrogacy costs, gestational surrogacy cost, surrogacy services, new hope, surrogacy, donor eggs, average cost of surrogacy, ivf, ivf ukraine, egg donation, donor egg Ukraine, fertility, clinic, cost ivf, cost surrogacy, agency, circle surrogacy",
  DESCRIPTION = 'Programs costs for surrogacy, IVF + ICSI and IVF with egg donation program in ukraine, loyal prices and good results!.',
  IMG = 'm_costs.jpg'
}

export enum seoAboutUsPage {
  TITLE = 'About agency - New Hope',
  KEYWORDS = "about new hope surrogacy, surrogacyukraine, gestational surrogacy, surrogacy agency, about agency, ivf ukraine, surr company of ukraine, kiev clinic, costs, programs costs, donor eggs, circle surrogacy, what they say about surrogacy",
  DESCRIPTION = 'Information about the company New Hope. New Hope provides surrogacy services in Australia, Canada, Germany, Ireland, USA',
  IMG = 'm_about.jpg'
}

export enum seoFaqPage {
  TITLE = 'FAQ surrogacy and donor eggs - New Hope',
  KEYWORDS = "faq, frequently asked questions, ivf and surrogacy services, surrogacyukraine, new hope agency, surrogacy, services, about, new hope surrogacy in ukraine, clinic, donor egg",
  DESCRIPTION = 'Frequently asked questions surrogacy in Ukraine, New Hope Surrogacy',
  IMG = 'm_faq.jpg'
}

export enum seoContactPage {
  TITLE = 'Contacts of New Hope',
  KEYWORDS = "contacts, contacts of new hope, surrogacy, surrogacyukraine, agency ukraine, new hope, contact surrogacy agency in Ukraine, 19a dniprovska embankment, office 203, kiev ukraine, best surrogacy agency in Kiev, IVF clinics Ukraine, fertility clinics Ukraine, best IVF clinics in Ukraine, best IVF clinics in Kiev, best ivf clinic kiev, best ivf clinic ukraine, egg donor Ukraine, egg donation USA, egg donor Ukraine, new hope agency, new hope surrogacy, new hope surrogacy ukraine, new hope ukraine, new hope kiev",
  DESCRIPTION = 'Contacts of New Hope - Egg donor and Surrogacy agency in Ukraine',
  IMG = 'm_contact.jpg'
}

export enum seoPrivacyPage {
  TITLE = 'Privacy Policy - New Hope',
  KEYWORDS = "privacy policy of surrogacy, new hope, surrogacyukraine, agency ukraine, legal, clinic, onor egg",
  DESCRIPTION = 'Privacy policy, New Hope Surrogacy offers an inexpensive IVF Program in Ukraine. IVF costs in Ukraine are much lower than in the USA or Canada.',
  IMG = 'm_legal.jpg'
}

export enum seoBlogPage {
  TITLE = 'Blog Surrogacy - New Hope',
  KEYWORDS = "fertility treatment, infertility, ivf, surrogacy, surrogacyukraine, egg donation, surrogacy agency ukraine, new hope agency, blog, surrogacy in ukraine",
  DESCRIPTION = 'blog and news about fertility treatments, infertility, surrogacy laws, ivf, egg donation in Ukrainian clinic New Hope.',
  IMG = 'm_blog.jpg'
}

export enum seoNgsPage {
  TITLE = 'NGS - New Hope',
  KEYWORDS = "ngs testing, test, ngs in ukraine, surrogacyukraine, surrogacy services next-generation sequencing ukraine, next-generation, ukraine, ngs (cgh), genome sequencing, next-generation dna sequencing (ngs), dna sequencing",
  DESCRIPTION = 'NGS (CGH) is a highly advanced scientific technique to test and diagnose embryos for specific genetic or chromosomal abnormalities.',
  IMG = 'gen-1.jpg'
}

export enum seoPgdPage {
  TITLE = 'PGD testing - New Hope',
  KEYWORDS = "pgd, pgd in Ukraine, preimplantation genetic diagnosis, pgd testing, ivf cycle, embryo transfer, culture and biopsy, fertilization, pgd testing cost, cost, testing cycle explained,  ivf ukraine, egg donation ukraine, donor egg ukraine,",
  DESCRIPTION = 'pgd is designed to detect specific genetic diseases that might be passed down to your biological child through the examination of dna.',
  IMG = 'gen-2.jpg'
}

export enum seoNiptPage {
  TITLE = 'Nipt Testing - New Hope',
  KEYWORDS = "noninvasive prenatal testing (nipt), nipt test, nipt testing, cost, nipt testing in ukraine, noninvasive prenatal testing, g-nipt, major chromosomal aneuploidies, prenatal diagnosis, cell-free fetal dna, trisomy 21, cell/dna trafficking in pregnancy, explaining the nipt screen, about nipt test",
  DESCRIPTION = 'Noninvasive prenatal testing (NIPT) is a method of determining the risk that the fetus will be born with certain genetic abnormalities.',
  IMG = 'gen-3.jpg'
}

export enum seoSurrogacyProgramsPage {
  TITLE = 'Surrogacy process in Ukraine - New Hope',
  KEYWORDS = "surrogacy process, ukraine, ivf ukraine, egg donation, surrogacy agency, surrogacy in ukraine, usa, agency usa, surrogacy agency ukraine, surrogates ukraine, surrogacy agency europe, surrogacy agency in ukraine, ivf usa, gestational surrogacy ukraine, gestational surrogacy in ukraine, surrogate mother ukraine, surrogate mother from ukraine, surrogate mother costs, surrogate fees, surrogate fees ukraine, egg donation, egg donor ukraine, egg donor usa, ivf costs in ukraine, best ivf clinics in ukraine, donor egg ukraine, fertility clinic europe, low cost ivf, ivf europe, ivf in ukraine, ivf clinics ukraine, best ivf clinics ukraine, ivf agency ukraine, fertility clinic ukraine, fertility clinic kiev, ivf kiev, ivf success rate kiev, new hope agency, new hope surrogacy, new hope - surrogacy ukraine, new hope - ukraine, new hope -  kiev, surrogacy program ukraine, surrogacy with donor eggs ukraine, surrogacy pregnancy, gestational surrogacy ukraine",
  DESCRIPTION = 'Surrogacy process in Ukraine is absolutely legal. Unlike many other European countries Ukraine allows surrogacy.',
  IMG = 'm_doc.jpg'
}

export enum seo404Page {
  TITLE = 'Page not found - New Hope',
  KEYWORDS = "404, not found, surrogacy, ukraine, clinic, ivf, kiev clinic, average cost of surrogacy, ivf ukraine",
  DESCRIPTION = 'Our agency provides IVF and surrogacy services in a legal way to couples from all over the world.',
  IMG = 'm_main.jpg'
}

export enum seoTestimonialsPage {
  TITLE = 'Surrogacy reviews - New Hope',
  KEYWORDS = "testimonial surrogacy ukraine, reviews, surrogacy reviews, what they say about surrogacy, ivf ukraine, egg donation, surrogacy agency",
  DESCRIPTION = 'Real feedback from parents who took part in the surrogacy program in New Hope (Ukraine) - New Hope',
  IMG = 'm_reviews.jpg'
}






