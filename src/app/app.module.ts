import {Injector, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {RouterModule} from "@angular/router";
import {LayoutRootModule} from "@modules/layout-root/layout-root.module";
import {HttpClientModule, HTTP_INTERCEPTORS} from "@angular/common/http";
import {FlexLayoutModule} from "@angular/flex-layout";
import {SwiperModule} from "swiper/angular";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LoadingBarHttpClientModule} from '@ngx-loading-bar/http-client';
import {LoadingBarRouterModule} from '@ngx-loading-bar/router';
import {LoadingBarModule, LOADING_BAR_CONFIG} from '@ngx-loading-bar/core';
import {NgSelectModule} from '@ng-select/ng-select';
import {UiModule} from "@ui/ui.module";
import {RouteInterceptorService} from '@core/interceptors/route-interceptor.service';
import {NativeRefService} from "@core/refs/native-ref.service";
import {SharedModule} from "@shared/shared.module";
import {NgxSsrCacheModule} from "@ngx-ssr/cache";

// import {NgParticlesModule} from "ng-particles";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule.withServerTransition({appId: 'serverApp'}),
    NgxSsrCacheModule.configLruCache({maxAge: 10 * 60_000, maxSize: 50}),
    BrowserAnimationsModule,
    BrowserModule,
    RouterModule,
    LayoutRootModule,
    FlexLayoutModule.withConfig({ssrObserveBreakpoints: ['xs', 'gt-xs', 'lt-sm', 'lt-md',]}),
    SwiperModule,
    FormsModule,
    ReactiveFormsModule,
    LoadingBarModule,
    LoadingBarHttpClientModule,
    LoadingBarRouterModule,
    NgSelectModule,
    UiModule,
    // NgParticlesModule,
    SharedModule,
    AppRoutingModule,
    // RouterModule.forRoot(routes, {
    //   onSameUrlNavigation: 'reload'
    // })
  ],
  providers: [
    NativeRefService,
    RouteInterceptorService,
    {
      provide: {LOADING_BAR_CONFIG},
      useValue: {latencyThreshold: 0}
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
